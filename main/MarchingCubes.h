#ifndef MARCHINGCUBES_H
#define MARCHINGCUBES_H

#include "MyResources.h"

class MarchingCubes
{
public:
    MarchingCubes(glm::ivec3 gridSize, std::vector<int>* volumeData);

    void generateMeshCPU(GLint isolevel);

    std::vector<GLfloat> getTrianglesPositionList();
    std::vector<GLfloat> getTrianglesNormalsList();
    std::vector<GLfloat> getTrianglesUVList();
    inline GLuint getVerticeCount() { return static_cast<GLuint>(getTrianglesPositionList().size() / 3.0f); }

private:
    /*************************************************************************
    * private methods
    *************************************************************************/
    void calculateCell();

    GLint getDataValue(GLint x, GLint y, GLint z);
    GLfloat getDataValueInterpolated(GLfloat x, GLfloat y, GLfloat z);

    GLint getTriTableValue(GLint x, GLint y);

    glm::vec3 vertexInterp(glm::vec3 v0, glm::vec3 v1, GLint v0Val, GLint v1Val);

    void initCornerValues();

    int cellValue();

    glm::vec3 gradient(glm::vec3 pos);

    void addVertice(GLint value, GLint i);


    /*************************************************************************
    * private variables
    *************************************************************************/

    GLint iCellX, iCellY, iCellZ;

    std::vector<int>* iVolumeData;

    glm::ivec3 ivGridSize;
    GLint iIsolevel;

    glm::vec3 vCorner[8];
    glm::vec4 vEdgePositions[12];
    GLint iCornerValues[8];

    float m_minX;
    float m_maxX;
    float m_minY;
    float m_maxY;
    float m_minZ;
    float m_maxZ;

    std::vector<GLfloat> vTrianglesPositionList;
    std::vector<GLfloat> vTrianglesNormalsList;
    std::vector<GLfloat> vTrianglesUVList;

};

#endif // MARCHINGCUBES_H
