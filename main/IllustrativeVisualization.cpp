#include "IllustrativeVisualization.h"

bool IllustrativeVisualization::USE_GOOCH_SHADING = false;
bool IllustrativeVisualization::USE_CONTOURS_SHADING = false;
bool IllustrativeVisualization::USE_HATCHING = false;
bool IllustrativeVisualization::USE_CELSHADING = false;
bool IllustrativeVisualization::USE_STIPPLING = false;

GLuint uiSimpleRenderProgram;

IllustrativeVisualization::IllustrativeVisualization(){
	simpleShading = new SimpleShading();
	goochShading = new GoochShading();
	contoursShading = new ContoursShading();
	hatching = new Hatching();
	celShading = new CelShading();
	stippling = new Stippling();

	vLightPosition = glm::vec3(2.0f, 2.0f, 2.0f);
}

IllustrativeVisualization::~IllustrativeVisualization(){
	delete simpleShading;
	delete goochShading;
	delete contoursShading;
	delete hatching;
	delete celShading;
	delete stippling;

	glDeleteProgram(uiSimpleRenderProgram);
}

void IllustrativeVisualization::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint uvBuffer, GLuint vertexCount, glm::mat4 modelMatrix){

	//Render Pass Program
	MyOpenGL::SHADER SHADER_SIMPLE_VERTEX(SHADERS_PATH "/Visualisation/VisualisationVertex.glsl", GL_VERTEX_SHADER);
	MyOpenGL::SHADER SHADER_SIMPLE_FRAGMENT(SHADERS_PATH "/Visualisation/VisualisationFragment.glsl", GL_FRAGMENT_SHADER);

	MyOpenGL::SHADER simpleShader[2];
	simpleShader[0] = SHADER_SIMPLE_VERTEX;
	simpleShader[1] = SHADER_SIMPLE_FRAGMENT;
	uiSimpleRenderProgram = MyOpenGL::createProgram(simpleShader, 2);

	MyOpenGL::SHADER SHADER_STIPPLING_VERTEX(SHADERS_PATH "/Stippling/stippling.vert", GL_VERTEX_SHADER);
    MyOpenGL::SHADER SHADER_STIPPLING_FRAGMENT(SHADERS_PATH "/Stippling/stippling.frag", GL_FRAGMENT_SHADER);

    MyOpenGL::SHADER simpleShaders[2];
    simpleShaders[0] = SHADER_STIPPLING_VERTEX;
    simpleShaders[1] = SHADER_STIPPLING_FRAGMENT;
    GLuint uiStipplingProgram = MyOpenGL::createProgram(simpleShaders, 2);
	
	//Init Simple Shading
	simpleShading->initData(meshBuffer, normalBuffer, vertexCount, modelMatrix, uiSimpleRenderProgram);
	simpleShading->setLightPosition(&vLightPosition);

	//Init Gooch Shading
	goochShading->initData(meshBuffer, normalBuffer, vertexCount, modelMatrix, uiSimpleRenderProgram);
	goochShading->setLightPosition(&vLightPosition);

	//Init Contours Shading
	contoursShading->initData(meshBuffer, normalBuffer, vertexCount, modelMatrix, uiSimpleRenderProgram);

	//Init Hatching
	hatching->initData(meshBuffer, normalBuffer, vertexCount, modelMatrix, uiSimpleRenderProgram);
	hatching->setLightPosition(&vLightPosition);

	//Init Cel Shading
	celShading->initData(meshBuffer, normalBuffer, vertexCount, modelMatrix, uiSimpleRenderProgram);
	celShading->setLightPosition(&vLightPosition);
	
	stippling->initData(meshBuffer, normalBuffer, uvBuffer, vertexCount, modelMatrix, uiStipplingProgram);
    stippling->setLightPosition(&vLightPosition);
}

void IllustrativeVisualization::render(Camera* camera, GLuint windowWidth, GLuint windowHeight, GLuint mode){
	switch (mode)
	{
	case MyEnums::SIMPLE_SHADING:
		renderSimpleShading(camera, windowWidth, windowHeight);
		break;
	case MyEnums::ILLUSTRATIVE_VISUALIZATION:
		renderIllustrative(camera, windowWidth, windowHeight);
		break;
	default:
		renderSimpleShading(camera, windowWidth, windowHeight);
		break;
	}
}

void IllustrativeVisualization::renderIllustrative(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	if(IllustrativeVisualization::USE_GOOCH_SHADING)
		goochShading->render(camera, windowWidth, windowHeight);

	if ((IllustrativeVisualization::USE_GOOCH_SHADING && IllustrativeVisualization::USE_CONTOURS_SHADING) 
		|| IllustrativeVisualization::USE_HATCHING || IllustrativeVisualization::USE_STIPPLING)
		contoursShading->fillDepthBuffer(camera, windowWidth, windowHeight);

	if (IllustrativeVisualization::USE_CELSHADING)
		celShading->render(camera, windowWidth, windowHeight);

	if(IllustrativeVisualization::USE_CONTOURS_SHADING)
		contoursShading->render(camera, windowWidth, windowHeight);

	if(IllustrativeVisualization::USE_HATCHING)
		hatching->render(camera,windowWidth,windowHeight);
	
	if (IllustrativeVisualization::USE_STIPPLING)
        stippling->render(camera, windowWidth, windowHeight);

}

void IllustrativeVisualization::renderSimpleShading(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	simpleShading->render(camera, windowWidth, windowHeight);
}