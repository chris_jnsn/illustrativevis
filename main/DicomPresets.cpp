#include "DicomPresets.h"
#include <iostream>
#include<fstream>

std::vector<DicomPreset*> DicomPresets::DICOM_PRESETS = std::vector<DicomPreset*>();
int DicomPresets::NUMBER_OF_PRESETS = 0;

    void DicomPresets::loadPresetsFromFile()
    {

        std::string name;
        std::string filePath;
        int numOfSlices;

        std::ifstream file;
        file.open((DICOM_PATH + std::string("/dicomPresets.txt")).c_str());

        std::string line;

        if (file.is_open())
        {
            while (file.good())
            {
                getline(file, line);

                std::size_t startIndex = line.find("DICOM(");
                std::size_t endIndex;
                if (startIndex == 0 && startIndex != std::string::npos)
                {
                    std::cout << "================================================================================================================================================" << std::endl;
                    std::cout << "Loading new DICOM Preset:" << std::endl;
                    startIndex += std::string("DICOM(").length();

                    endIndex = line.find(";", startIndex + 1);
                    if (endIndex == std::string::npos)
                    {
                        std::cout << "Error in DicomPresets::loadPresetsFromFile() - Dataset name not found!" << std::endl;
                        std::cout << "================================================================================================================================================" << std::endl;
                        continue;
                    }
                    name = line.substr(startIndex, endIndex - startIndex); std::cout << "Name: " << name << std::endl;

                    startIndex = ++endIndex;
                    endIndex = line.find(";", startIndex + 1);
                    if (endIndex == std::string::npos)
                    {
                        std::cout << "Error in DicomPresets::loadPresetsFromFile() - path not found!" << std::endl;
                        std::cout << "================================================================================================================================================" << std::endl;
                        continue;
                    }
                    filePath = DICOM_PATH + std::string("/");
                    filePath += line.substr(startIndex, endIndex - startIndex); std::cout << "Path: " << filePath << std::endl;

                    startIndex = ++endIndex;
                    endIndex = line.find(")", startIndex + 1);
                    if (endIndex == std::string::npos)
                    {
                        std::cout << "Error in DicomPresets::loadPresetsFromFile() - number of slices not found!" << std::endl;
                        std::cout << "================================================================================================================================================" << std::endl;
                        continue;
                    }
                    std::string sliceCountStr = line.substr(startIndex, endIndex - startIndex);
                    numOfSlices = atoi(sliceCountStr.c_str()); std::cout << "Number Of Slices: " << numOfSlices << std::endl;

                    if (numOfSlices <= 0)
                    {
                        std::cout << "Error in DicomPresets::loadPresetsFromFile() - number of slices " << numOfSlices << " is negative or zero!" << std::endl;
                    continue;
                    }

                    DICOM_PRESETS.push_back(new DicomPreset(name,filePath,numOfSlices));
                    std::cout << "================================================================================================================================================" << std::endl;

                }
            }
        }

        DicomPresets::NUMBER_OF_PRESETS = DICOM_PRESETS.size();
    }
