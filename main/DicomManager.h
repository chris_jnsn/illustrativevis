#ifndef DICOMMANAGER_H
#define DICOMMANAGER_H

#include <iostream>


#include "imebra/include/imebra.h"
#include <sstream>

#ifdef PUNTOEXE_WINDOWS
#include <process.h>
#else
#include <spawn.h>
#include <sys/wait.h>
#endif

#include <memory>
#include <list>

#include "math.h"
#include "MyResources.h"

class DicomManager
{
public:
    DicomManager(void);
	~DicomManager(void);

    void readData(std::string dataPath);
	void filterImageData(int kernelSize);
    void clearImagaData();	

    std::vector<imbxInt32> getImageData();
    imbxUint32 getImageWidth();
    imbxUint32 getImageHeight();

    double getPixelSpacing();
    double getSliceSpacing();

	glm::mat4 getModelMatrix();

private:
	std::vector<glm::vec3> vPosition;
    std::vector<imbxInt32> iImageData;

    imbxUint32 uiWidth;
    imbxUint32 uiHeight;
	imbxUint32 uiSliceCount;

    double dPixelSpacing;
    double dSliceSpacing;
};

#endif // DICOMMANAGER_H
