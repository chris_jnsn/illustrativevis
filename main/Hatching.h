#ifndef HATCHING_H
#define HATCHING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class Hatching
{
public:
    Hatching();
    ~Hatching();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

    void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);

    void setLightPosition(glm::vec3 *lightPosition);

    //Parameter
    float m_theta;
    float m_patternSize;
    int m_intensity;
    glm::fvec3 m_hatchingColor;
    float m_hatchingAlpha;
    glm::fvec3 m_fillColor;
    float m_fillAlpha;
    bool m_useFillColor;

private:

    /*************************************************************************
    * private variables
    *************************************************************************/
    //General Settings
    glm::vec3 *vLightPosition;

    GLuint uiWindowWidth;
    GLuint uiWindowHeight;

    //Mesh Data
    GLuint uiMeshBuffer;
    GLuint uiNormalBuffer;
    GLuint uiVertexCount;
    glm::mat4 mModelMatrix;

    //Shader Programs
    GLuint renderProgram;
};

#endif // HATCHING_H