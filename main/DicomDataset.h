#pragma once

#include<string>
#include<vector>
#include<GL/glew.h>

#include "imebra/include/imebra.h"
#include <sstream>

#ifdef PUNTOEXE_WINDOWS
#include <process.h>
#else
#include <spawn.h>
#include <sys/wait.h>
#endif

class MarchingCubes;
class MarchingCubesGPU;
class DicomManager;

class DicomDataset {

public:

  DicomDataset(std::string const & datasetName, std::string const & path, unsigned int const & startSliceNumber, unsigned int const & endSliceNumber, signed int const & isovalue, unsigned int const & filterKernelSize);
  ~DicomDataset();

  inline std::string const &      getFileName()               { return m_fileName; }
  inline std::string const &      getFilePath()               { return m_filePath; }
  inline unsigned int             getSliceStartNum()          { return m_sliceStartNum; }
  inline unsigned int             getSliceEndNum()            { return m_sliceEndNum; }
  inline unsigned int             getSliceCount()             { return m_sliceCount; }
  inline MarchingCubes &          getMarchingCubes()          { return *m_marchingCubes; }
  inline DicomManager &           getDicomManager()           { return *m_dicomManager; }
  inline GLuint const &           getMeshBuffer()             { return m_meshBuffer; }
  inline GLuint const &           getNormalBuffer()           { return m_normalBuffer; }
  inline GLuint const &           getUVBuffer()               { return m_uvBuffer; }
  inline signed int               getIsovalue()               { return m_isovalue; }
  inline unsigned int             getFilterKernelSize()       { return m_filterKernelSize; }
  inline signed int               setIsovalue(int const val)  { m_isovalue = val; }
  
  void changeIsovalue(signed int const newVal);

  GLuint getVerticeCount();
private:

  void loadData();
  void marchTheCubes();

  std::string m_fileName;
  std::string m_filePath;
  unsigned int const m_sliceStartNum;
  unsigned int const m_sliceEndNum;
  unsigned int const m_sliceCount;
  signed int m_isovalue;
  unsigned int const m_filterKernelSize;
  MarchingCubes *m_marchingCubes;
  MarchingCubesGPU *m_marchingCubesGPU;
  DicomManager *m_dicomManager;
  GLuint m_meshBuffer;
  GLuint m_normalBuffer;
  GLuint m_uvBuffer;
  std::vector<imbxInt32> m_imageData;

};
