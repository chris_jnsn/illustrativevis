/*
 * Information:
 *
 * This software is the result of an assignment as part of the lecture visual computing for medicine at the university Koblenz-Landau with the title "Illustrative Visualization"
 *
 * Members of the assignment: Jan Beutgen, Christopher Jensen, Dennis Ermtraud
 *
 * Task distribution for evaluation purposes:
 *
 * Beutgen: project management (CMake & Git), software architecture, data loading, GUI, Hatching, Stippling, Documentation
 * Jensen: GUI, Cel Shading with included Antialiasing, version control and merging, shader merging
 * Ermtraud: Marching Cubes, DICOM management, preprocessing of data (filtering), Simple Shading, Contours, Gooch-Shading
 *
 */

#include <time.h>

#include "MyResources.h"

#include "DicomManager.h"
#include "MarchingCubes.h"
#include "MarchingCubesGPU.h"
#include "Camera.h"
#include "MyOpenGL.h"
#include "DicomDataset.h"
#include "MainGUI.h"
#include "DicomImportGUI.h"
#include "IllustrativeVisualization.h"
#include "DicomPresets.h"

using namespace std;

MainGUI *m_gui;
DicomImportGUI *m_dicomImportGUI;
LoadingScreenGUI *m_loadingScreenGUI;
bool fTenPressed = false;
bool bUseGPU = false;
bool mouseClickOnGUI = false;
GLFWwindow* mainWindow;
Camera* camera;
double renderTime;

std::vector<DicomDataset*> loadedDataSets;
DicomDataset *currentDataset;

GLint newIsolevel;
GLboolean wireframe = GL_FALSE;
glm::vec3 clearColor = glm::vec3(0.8f, 0.8f, 0.8f);
GLuint visualizationMode = MyEnums::SIMPLE_SHADING;

IllustrativeVisualization* illustrativeVisualization;

GLint m_width = 1000;
GLint m_height = 800;

struct mousePos {
	mousePos(){
		dPrevX = 0.0;
		dPrevY = 0.0;
		dCurX = 0.0;
		dCurY = 0.0;
	}

	mousePos(GLdouble curX, GLdouble curY){
		dPrevX = curX;
		dPrevY = curY;
		dCurX = curX;
		dCurY = curY;
	}

	GLdouble dPrevX;
	GLdouble dPrevY;
	GLdouble dCurY;
	GLdouble dCurX;
};

void renderDicomImport()
{
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	m_dicomImportGUI->render(mainWindow);
	glfwSwapBuffers(mainWindow);

	if(glfwGetKey(mainWindow,GLFW_KEY_ENTER) || glfwGetKey(mainWindow,GLFW_KEY_KP_ENTER))
		m_dicomImportGUI->setLoadButtonPressed(true);

	if (m_dicomImportGUI->getLoadButtonPressed())
	{
		//check if dataset is allready loaded
		for (std::vector<DicomDataset*>::iterator it = loadedDataSets.begin(); it != loadedDataSets.end(); ++it)
		{
            if ((*it)->getFileName() == m_dicomImportGUI->getFileName())
            {
                currentDataset = *it;

                //If some changes were made (isovalue, slice count etc..) the dataset has to be reloaded
                if (currentDataset->getSliceStartNum() != m_dicomImportGUI->getSliceStart() || 
					currentDataset->getSliceEndNum() != m_dicomImportGUI->getSliceEnd() || 
					currentDataset->getIsovalue() != m_dicomImportGUI->getIsovalue())
                {
                    loadedDataSets.erase(it);
                    delete currentDataset;
                    currentDataset = nullptr;
                    break;
                }
            }
			
		}

		//otherwise import new dataset
		if (!currentDataset)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glEnable(GL_DEPTH_TEST);
			m_loadingScreenGUI->render(mainWindow);
			glfwSwapBuffers(mainWindow);

			DicomDataset *newDataset = new DicomDataset(m_dicomImportGUI->getFileName(), m_dicomImportGUI->getFilePath(),
				m_dicomImportGUI->getSliceStart(), m_dicomImportGUI->getSliceEnd(), m_dicomImportGUI->getIsovalue(), 
				m_dicomImportGUI->getFilterKernelSize());

			loadedDataSets.push_back(newDataset);
			currentDataset = newDataset;

			newIsolevel = currentDataset->getIsovalue();

		}
		
		//IllustrativeVisualization
        illustrativeVisualization->initData(currentDataset->getMeshBuffer(), currentDataset->getNormalBuffer(), currentDataset->getUVBuffer(),
			currentDataset->getVerticeCount(), currentDataset->getDicomManager().getModelMatrix());
		//------
	}

}

void resize(){
	int width, height;
	glfwGetWindowSize(mainWindow, &width, &height);

	if (m_width != width || m_height != height){
		m_width = width;
		m_height = height;
		camera->setAspectRatio(GLfloat(width) / GLfloat(height));
		glViewport(0, 0, width, height);
	}
}


void renderDataset(double currenTime)
{
	glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0f);
	//Render Mesh
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	if (glfwGetKey(mainWindow, GLFW_KEY_F10))
		fTenPressed = true;

	if (fTenPressed && !glfwGetKey(mainWindow, GLFW_KEY_F10))
	{
		if (m_gui->isVisible())
			m_gui->hide();
		else
			m_gui->show();

		fTenPressed = false;
	}

    if (wireframe == GL_FALSE)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		m_gui->setPolygonMode(1);
	}
    else if (wireframe == GL_TRUE)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		m_gui->setPolygonMode(0);
	}

	illustrativeVisualization->render(camera, m_width, m_height, visualizationMode);

	//----------------------------

	m_gui->render(mainWindow);

	renderTime = glfwGetTime() - currenTime;

	glfwSwapBuffers(mainWindow);
}

int main(int argc, char *argv[])
{
	//load datasets from file
	DicomPresets::loadPresetsFromFile();

	currentDataset = nullptr;
	newIsolevel = 150;

	glfwInit();

	glfwWindowHint(GLFW_SAMPLES, 4);
	mainWindow = glfwCreateWindow(m_width, m_height, "Illustrative Visualization", NULL, NULL);
	glfwMakeContextCurrent(mainWindow);

	//init opengl 3 extensions
	GLenum GlewInitResult;
	GlewInitResult = glewInit();

	if (GLEW_OK != GlewInitResult) {
		fprintf(stderr,
			"ERROR: %s\n",
			glewGetErrorString(GlewInitResult));
		exit(EXIT_FAILURE);
	}

	//Basic Setup:
	glViewport(0, 0, m_width, m_height);
	//Set color to use when clearing the background.
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);

	//Turn on backface culling
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	//Turn on depth testing
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


	//init Camera
	camera = new Camera();
	camera->setProjection(60.0f, GLfloat(m_width) / GLfloat(m_height), 0.1f, 100.f);
	camera->setCamera(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	//------------------------------------------------------

	//Init Mouse Position
	GLdouble initPosX, initPosY;
	glfwGetCursorPos(mainWindow, &initPosX, &initPosY);
	mousePos mouse(initPosX, initPosY);

	int fps;

	m_gui = new MainGUI();
	m_gui->setFPSVariable(&fps, &renderTime);
	m_gui->setIsolevelVariable(&newIsolevel);
    m_gui->setWireframeVariable(reinterpret_cast<bool*>(&wireframe));
	

	m_dicomImportGUI = new DicomImportGUI();
	m_loadingScreenGUI = new LoadingScreenGUI();

	double startTime = glfwGetTime();

	m_gui->setVisualizationMode(&visualizationMode);
	m_gui->setClearColor(&clearColor);

	//init Feature Lines
	illustrativeVisualization = new IllustrativeVisualization();
	m_gui->setIllustrativeVisualization(illustrativeVisualization);
	m_gui->setLightPosition(&illustrativeVisualization->vLightPosition);

	//Simple Shading GUI
	m_gui->setRenderModeVariable(&illustrativeVisualization->getSimpleShading()->uiRenderMode);

	//Gooch Shading GUI
	m_gui->setGoochSamples(&illustrativeVisualization->getGoochShading()->uiSamples);

	m_gui->setGoochLineThickness(&illustrativeVisualization->getGoochShading()->fLineThickness);
	m_gui->setGoochSobelTreshhold(&illustrativeVisualization->getGoochShading()->fSobelTreshhold);

	m_gui->setGoochDiffuseColor(&illustrativeVisualization->getGoochShading()->vDiffuseColor);
	m_gui->setGoochSpecularColor(&illustrativeVisualization->getGoochShading()->vSpecularColor);
	m_gui->setGoochAmbientColor(&illustrativeVisualization->getGoochShading()->vAmbientColor);

	m_gui->setGoochCoolColor(&illustrativeVisualization->getGoochShading()->vGoochCoolColor);	
	m_gui->setGoochWarmColor(&illustrativeVisualization->getGoochShading()->vGoochWarmColor);

	m_gui->setGoochDiffuseCoolInfluence(&illustrativeVisualization->getGoochShading()->fDiffuseCoolInfluence);
	m_gui->setGoochDiffuseWarmInfluence(&illustrativeVisualization->getGoochShading()->fDiffuseWarmInfluence);

	//Contours Shading
	m_gui->setContoursLineThickness(&illustrativeVisualization->getContoursShading()->fLineThickness);
	m_gui->setContoursLineColor(&illustrativeVisualization->getContoursShading()->vLineColor);
	m_gui->setContoursSolidColor(&illustrativeVisualization->getContoursShading()->vSolidColor);
	m_gui->setContoursSolidAlpha(&illustrativeVisualization->getContoursShading()->fSolidAlpha);
	m_gui->setContoursUseSolidColor(&illustrativeVisualization->getContoursShading()->useSolidColor);
	//------

	//Hatching
	m_gui->setHatchingTheta(&illustrativeVisualization->getHatching()->m_theta);
	m_gui->setHatchingPatternSize(&illustrativeVisualization->getHatching()->m_patternSize);
	m_gui->setHatchingIntensity(&illustrativeVisualization->getHatching()->m_intensity);
	m_gui->setHatchingColor(&illustrativeVisualization->getHatching()->m_hatchingColor);
	m_gui->setHatchingAlpha(&illustrativeVisualization->getHatching()->m_hatchingAlpha);
	m_gui->setHatchingFillColor(&illustrativeVisualization->getHatching()->m_fillColor);
	m_gui->setHatchingFillAlpha(&illustrativeVisualization->getHatching()->m_fillAlpha);
	m_gui->setHatchingUseFillColor(&illustrativeVisualization->getHatching()->m_useFillColor);

    //Stippling
    m_gui->setStipplingColor(&illustrativeVisualization->getStippling()->m_stipplingColor);
    m_gui->setStipplingAlpha(&illustrativeVisualization->getStippling()->m_stipplingAlpha);
    m_gui->setStipplingFillColor(&illustrativeVisualization->getStippling()->m_fillColor);
    m_gui->setStipplingFillAlpha(&illustrativeVisualization->getStippling()->m_fillAlpha);
	m_gui->setStipplingUseFillColor(&illustrativeVisualization->getStippling()->m_useFillColor);
	m_gui->setStipplingPointer(illustrativeVisualization->getStippling());

	//Cel Shading
	m_gui->setCelDiffuseMaterial(&illustrativeVisualization->getCelShading()->m_diffuseMaterial);
	m_gui->setCelAmbientMaterial(&illustrativeVisualization->getCelShading()->m_ambientMaterial);
	m_gui->setCelSpecularMaterial(&illustrativeVisualization->getCelShading()->m_specularMaterial);
	m_gui->setCelShininess(&illustrativeVisualization->getCelShading()->m_shininess);


	bool mouseJustPressed = false;
	while (!glfwWindowShouldClose(mainWindow) && !glfwGetKey(mainWindow,GLFW_KEY_ESCAPE)) {

		if(!glfwGetMouseButton(mainWindow,GLFW_MOUSE_BUTTON_1) && mouseJustPressed)
			mouseJustPressed = false;

		if(glfwGetMouseButton(mainWindow,GLFW_MOUSE_BUTTON_1) && !mouseJustPressed)
		{
			mouseJustPressed = true;
			double xPos, yPos;
			glfwGetCursorPos(mainWindow, &xPos, &yPos);

			if(xPos > m_width - m_gui->getWidth())
			{
				mouseClickOnGUI = true;
			}

			else
			{
				mouseClickOnGUI = false;
			}
		}

		float deltaT = static_cast<float>(glfwGetTime() - startTime);
		startTime = glfwGetTime();

		fps = static_cast<int>(1.0f / deltaT);

		if(!mouseClickOnGUI) {
			//Mouse Movement
			glfwGetCursorPos(mainWindow, &mouse.dCurX, &mouse.dCurY);

			//250 is the width of the gui side panel
			if (glfwGetMouseButton(mainWindow, GLFW_MOUSE_BUTTON_LEFT)) {
				GLfloat offsetX = mouse.dCurX - mouse.dPrevX;
				GLfloat offsetY = mouse.dCurY - mouse.dPrevY;
				camera->rotateCamera(-offsetX, -offsetY);
			}
			if (glfwGetMouseButton(mainWindow, GLFW_MOUSE_BUTTON_RIGHT)) {
				GLfloat zoomValue = (mouse.dCurY - mouse.dPrevY) * 0.2f;
				camera->zoomCamera(zoomValue);
			}

			mouse.dPrevX = mouse.dCurX;
			mouse.dPrevY = mouse.dCurY;
		}
		//Key Movement
		if (glfwGetKey(mainWindow, GLFW_KEY_W)) camera->moveCameraSphereUp();
		if (glfwGetKey(mainWindow, GLFW_KEY_A)) camera->moveCameraSphereLeft();
		if (glfwGetKey(mainWindow, GLFW_KEY_D)) camera->moveCameraSphereRight();
		if (glfwGetKey(mainWindow, GLFW_KEY_S)) camera->moveCameraSphereDown();
		if (glfwGetKey(mainWindow, GLFW_KEY_Q)) camera->moveCameraSphereForward();
		if (glfwGetKey(mainWindow, GLFW_KEY_E)) camera->moveCameraSphereBack();

		glfwPollEvents();

		if (!currentDataset)
		{
			renderDicomImport();
		}
		else
		{

			if (m_gui->isoValueAdapted()){
				currentDataset->changeIsovalue(newIsolevel);

				//IllustrativeVisualization
                illustrativeVisualization->initData(currentDataset->getMeshBuffer(), currentDataset->getNormalBuffer(), currentDataset->getUVBuffer(),
					currentDataset->getVerticeCount(), currentDataset->getDicomManager().getModelMatrix());
				//------


			}

			renderDataset(startTime);

			if (m_gui->loadNewDataset())
				currentDataset = nullptr;

		}

		resize();
	}
	glfwDestroyWindow(mainWindow);
	glfwTerminate();
}
