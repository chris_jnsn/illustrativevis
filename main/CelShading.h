#ifndef CELSHADING_H
#define CELSHADING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class CelShading
{
public:
	CelShading();
	~CelShading();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

	void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	void setLightPosition(glm::vec3 *lightPosition);

	//Parameter
	glm::vec3 m_diffuseMaterial;
	glm::vec3 m_ambientMaterial;
	glm::vec3 m_specularMaterial;
	
	float m_shininess;

private:

	/*************************************************************************
	* private variables
	*************************************************************************/
	//General Settings
	glm::vec3 *vLightPosition;
	GLuint uiWindowWidth;
	GLuint uiWindowHeight;

	//Mesh Data
	GLuint uiMeshBuffer;
	GLuint uiNormalBuffer;
	GLuint uiVertexCount;
	glm::mat4 mModelMatrix;

	//Shader Programs
	GLuint renderProgram;
};

#endif // CELSHADING_H
