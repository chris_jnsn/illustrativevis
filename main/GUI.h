#pragma once
#include<string>
#include<vector>
#include<iostream>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<glm/glm.hpp>
#include<glm/gtc/constants.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include<glm/gtc/matrix_inverse.hpp>
#include<glm/gtc/matrix_access.hpp>

class GUI
{
  public:
    GUI();
    ~GUI();

    void initGUI();
    void update(GLFWwindow* window);
    void loadFonts();
    void drawElements(GLFWwindow* window);
    void tab();

	bool m_initialized;

  private:

};
