#include "SimpleShading.h"

using namespace glm;

SimpleShading::SimpleShading(){
	uiMeshBuffer = 0;
	uiNormalBuffer = 0;
	uiVertexCount = 0;
	mModelMatrix = mat4(1.0f);

	uiRenderMode = 2;
	vLightPosition = new vec3(2.0f, 2.0f, 2.0f);

	uiWindowWidth = 0;
	uiWindowHeight = 0;

	////Render First Pass Program
	//MyOpenGL::SHADER SHADER_SIMPLE_VERTEX(SHADERS_PATH "/Visualisation/VisualisationVertex.glsl", GL_VERTEX_SHADER);
	//MyOpenGL::SHADER SHADER_SIMPLE_FRAGMENT(SHADERS_PATH "/Visualisation/VisualisationFragment.glsl", GL_FRAGMENT_SHADER);

	//MyOpenGL::SHADER simpleShader[2];
	//simpleShader[0] = SHADER_SIMPLE_VERTEX;
	//simpleShader[1] = SHADER_SIMPLE_FRAGMENT;
	//uiSimpleRenderProgram = MyOpenGL::createProgram(simpleShader, 2);
}

SimpleShading::~SimpleShading(){

}

void SimpleShading::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
	uiMeshBuffer = meshBuffer;
	uiNormalBuffer = normalBuffer;
	uiVertexCount = vertexCount;
	mModelMatrix = modelMatrix;
	renderProgram = program;
}

void SimpleShading::render(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = renderProgram;
	glUseProgram(program);

	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glUniform1i(glGetUniformLocation(program, "useSimple"), true);

	glUniform1i(glGetUniformLocation(program, "uniformMode"), uiRenderMode);
	glUniform4f(glGetUniformLocation(program, "lightPosition"), vLightPosition->x, vLightPosition->y, vLightPosition->z, 1.0f);

	MyOpenGL::setMatrices(program, mModelMatrix, camera);

	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDisable(GL_DEPTH_TEST);

	glUniform1i(glGetUniformLocation(program, "useSimple"), false);
}

void SimpleShading::setLightPosition(glm::vec3 *lightPosition){
	vLightPosition = lightPosition;
}