#include "DicomImportGUI.h"
#include <imgui.h>
#include <GL/glew.h>

static bool mousePressed[2] = { false, false };

DicomImportGUI::DicomImportGUI() : GUI()
{
	
	m_datasetIndex = 0;

	m_compareIndex = 0;

	m_sliceStart = 0;


    if (DicomPresets::NUMBER_OF_PRESETS > 0)
        m_sliceEnd = DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->numOfSlices;
    else
        m_sliceEnd = 0;

	m_isovalue = 150;

	m_filterKernelSize = 3;

	m_loadButtonPressed = false;
}

DicomImportGUI::~DicomImportGUI()
{
    for (std::vector<DicomPreset*>::iterator it = DicomPresets::DICOM_PRESETS.begin(); it != DicomPresets::DICOM_PRESETS.end(); ++it)
	  delete *it;
}


void DicomImportGUI::render(GLFWwindow* window)
{
	
  glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

  if (!m_initialized)
    initGUI();

  ImGuiIO& io = ImGui::GetIO();

  mousePressed[0] = static_cast<bool>(glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_1));
  mousePressed[1] = static_cast<bool>(glfwGetMouseButton(window,GLFW_MOUSE_BUTTON_2));

  update(window);

  bool t = true;

  static bool no_titlebar   = true;
  static bool no_resize     = true;
  static bool no_move       = true;
  static bool no_scrollbar  = false;
  static bool no_collapse   = false;

  //static float bg_alpha = 1.0f;
  static float bg_alpha = 1.0f;

  ImGuiWindowFlags  window_flags = 0;
  if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
  if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
  if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
  if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
  if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;


  int width;
  int height;

  glfwGetWindowSize(window, &width, &height);

  float GUIWidth = static_cast<float> (width / 2.5f);
  float GUIHeight = static_cast<float> (height / 3.0f);
  

  ImGui::SetNextWindowPos(ImVec2(static_cast<float>(width / 2.0f) - GUIWidth / 2.0f, static_cast<float>(height / 2.0f) - GUIHeight/2.0f));
  ImGui::SetNextWindowSize(ImVec2(GUIWidth, GUIHeight));

  if(!ImGui::Begin(" ", &t, ImVec2(0, 0), bg_alpha, window_flags))
    ImGui::End();
  else
  {
    drawElements(window);
    ImGui::End();
  }

  glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);

  ImGui::Render();
}

void DicomImportGUI::drawElements(GLFWwindow* window)
{
    if (DicomPresets::NUMBER_OF_PRESETS == 0)
    {
        ImGui::Spacing();
        ImGui::Spacing();
        tab(); tab(); ImGui::Text("No Dicom Presets defined!");
        tab(); tab(); ImGui::Text("Go to resources/DICOM/dicomPresets.txt");
        tab(); tab(); ImGui::Text("and define a dataset!");
        return;
    }

	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	ImGui::Spacing();
	tab();
	tab();
	ImGui::SameLine();

    const char* items[10];
	
    for (int i = 0; i < static_cast<int>(DicomPresets::DICOM_PRESETS.size()); i++)
    {
        if (i > 15)
        {
            std::cout << "Error in DicomImportGUI::drawElements() - maximum number of Dicom Datasets is 15" << std::endl;
            break;
        }
        items[i] = DicomPresets::DICOM_PRESETS.at(i)->fileName.c_str();
	}

    ImGui::Combo("Dataset", &m_datasetIndex, items, static_cast<int>(DicomPresets::DICOM_PRESETS.size()));
	if (m_datasetIndex != m_compareIndex)
	{
		m_sliceStart = 0;
        m_sliceEnd = DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->numOfSlices;
		m_compareIndex = m_datasetIndex;
	}

	ImGui::Spacing();
	ImGui::Spacing();
	tab();
	tab();
	ImGui::SameLine();
	ImGui::InputInt("Slice Start", &m_sliceStart, 1, 10);

	if (m_sliceStart < 0)
		m_sliceStart = 0;
	else if (m_sliceStart >= m_sliceEnd)
		m_sliceStart = m_sliceEnd - 1;

	tab();
	tab();
	ImGui::SameLine();
	ImGui::InputInt("Slice End", &m_sliceEnd, 1, 10);
    
    if (DicomPresets::NUMBER_OF_PRESETS > 0)
    {
        if (m_sliceEnd > DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->numOfSlices)
            m_sliceEnd = DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->numOfSlices;
        else if (m_sliceStart >= m_sliceEnd)
            m_sliceEnd = m_sliceStart + 1;
    }

    else
    {
        m_sliceEnd = 0;
        m_sliceStart = 0;
    }
    

	ImGui::Spacing();
	ImGui::Spacing();
	tab();
	tab();
	ImGui::SameLine();
	ImGui::InputInt("Isovalue", &m_isovalue, 1, 10);

	if (m_isovalue > 3000)
		m_isovalue = 3000;
	else if (m_isovalue < -999)
		m_isovalue = -999;

	ImGui::Spacing();
	ImGui::Spacing();
	tab();
	tab();
	ImGui::SameLine();
	ImGui::InputInt("Filter Kernel", (int*)&m_filterKernelSize, 2, 4);

	if (m_filterKernelSize > 29)
		m_filterKernelSize = 29;
	else if (m_filterKernelSize < 1)
		m_filterKernelSize = 1;

	ImGui::Spacing();
	ImGui::Spacing();

	ImGui::Spacing();
	ImGui::Spacing();

	for (int i = 0; i < 38; i++)
		tab();

	if (ImGui::Button("Load"))
	{
		m_loadButtonPressed = true;
	}
}


void LoadingScreenGUI::render(GLFWwindow* window)
{

	glClearColor(0.2, 0.2, 0.2, 1.0);

	if (!m_initialized)
		initGUI();

	ImGuiIO& io = ImGui::GetIO();

	mousePressed[0] = static_cast<bool>(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1));
	mousePressed[1] = static_cast<bool>(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2));

	update(window);

	bool t = true;

	static bool no_titlebar = true;
	static bool no_resize = true;
	static bool no_move = true;
	static bool no_scrollbar = false;
	static bool no_collapse = false;

	//static float bg_alpha = 1.0f;
	static float bg_alpha = 0.0f;

	ImGuiWindowFlags  window_flags = 0;
	if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
	if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
	if (!no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;


	int width;
	int height;

	glfwGetWindowSize(window, &width, &height);

	float GUIWidth = static_cast<float> (width / 5.0f);
	float GUIHeight = static_cast<float> (height / 10.0f);


	ImGui::SetNextWindowPos(ImVec2(static_cast<float>(width / 2.0f) - GUIWidth / 2.0f, static_cast<float>(height / 2.0f) - GUIHeight / 2.0f));
	ImGui::SetNextWindowSize(ImVec2(GUIWidth, GUIHeight));

	if (!ImGui::Begin(" ", &t, ImVec2(0, 0), bg_alpha, window_flags))
		ImGui::End();
	else
	{
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		tab();
		tab();
		ImGui::Text("Loading Dataset...");


		ImGui::End();
	}

	glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);

	ImGui::Render();
}