#pragma once
#include<string>
#include<vector>
#include<iostream>
#include"GUI.h"
#include"DicomPresets.h"

class DicomImportGUI : public GUI
{
public:
	DicomImportGUI();
	~DicomImportGUI();

	void render(GLFWwindow* window);
	inline signed int getSliceStart(){ return m_sliceStart; }
	inline signed int getSliceEnd(){ return m_sliceEnd; }
	inline int getIsovalue(){ return m_isovalue; }
	unsigned int getFilterKernelSize(){ return m_filterKernelSize; }
	inline void setLoadButtonPressed(bool state) {m_loadButtonPressed = state;}

	inline bool getLoadButtonPressed()
	{ 
		if (m_loadButtonPressed)
		{
			m_loadButtonPressed = false;
			return true;
		}
		return false;
	}

	inline std::string getFileName(){ return DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->fileName; }
    inline std::string getFilePath(){ return DicomPresets::DICOM_PRESETS.at(m_datasetIndex)->filePath; }


private:

	void drawElements(GLFWwindow* window);

	int m_datasetIndex;
	int m_compareIndex;
	int m_isovalue;
	signed int m_sliceStart;
	signed int m_sliceEnd;
	int m_filterKernelSize;
	bool m_loadButtonPressed;
};

class LoadingScreenGUI : public GUI
{
public:
	inline  LoadingScreenGUI() {}
	inline ~LoadingScreenGUI() {}

	void render(GLFWwindow* window);

private:

	void drawElements(GLFWwindow* window);
};

