#pragma once
#include<string>
#include<vector>
#include<iostream>
#include"GUI.h"
#include "MyResources.h"
#include "IllustrativeVisualization.h"

class MainGUI : public GUI
{
public:
	MainGUI();
	~MainGUI();

	void show();
	void hide();

	inline bool isVisible() { return m_visible; }

	void render(GLFWwindow* window);
	void setFPSVariable(int *finalFPS, double *renderTime);
	void setIsolevelVariable(int *isolevel);
	void setWireframeVariable(bool *wireframe);
	void setRenderModeVariable(GLint *renderMode);
	void setPolygonMode(int polygonMode);
	bool isoValueAdapted();
	bool loadNewDataset();

	inline int getWidth() { return m_width; }

	void setIllustrativeVisualization(IllustrativeVisualization* illuVisu);
	void setVisualizationMode(GLuint *visualizationMode);
	void setClearColor(glm::vec3 * clearColor);
	void setLightPosition(glm::vec3 *lightPosition);

	//Gooch Shading
	void setGoochSamples(GLuint *samples);

	void setGoochLineThickness(GLfloat *lineThickness);
	void setGoochSobelTreshhold(GLfloat *sobelTreshhold);

	void setGoochDiffuseColor(glm::vec3 *diffuseColor);
	void setGoochSpecularColor(glm::vec3 *specularColor);
	void setGoochAmbientColor(glm::vec3 *ambientColor);

	void setGoochCoolColor(glm::vec3 *coolColor);
	void setGoochWarmColor(glm::vec3 *warmColor);

	void setGoochDiffuseCoolInfluence(GLfloat *coolInfluence);
	void setGoochDiffuseWarmInfluence(GLfloat *warmInfluence);

	//Contours Shading
	void setContoursLineThickness(GLfloat *lineThickness);
	void setContoursLineColor(glm::vec3 *lineColor);
	void setContoursSolidColor(glm::vec3 *solidColor);
	void setContoursSolidAlpha(GLfloat *solidAlpha);
	void setContoursUseSolidColor(bool *solidState);

	//Hatching
	void setHatchingTheta(float *theta);
	void setHatchingPatternSize(float *patternSize);
	void setHatchingIntensity(int *intensity);
	void setHatchingColor(glm::vec3 *color);
	void setHatchingAlpha(float *alpha);
	void setHatchingFillColor(glm::vec3 *fillColor);
	void setHatchingFillAlpha(float *fillAlpha);
	void setHatchingUseFillColor(bool *useFillColor);

    //Stippling
    void setStipplingColor(glm::vec3 *color);
    void setStipplingAlpha(float *alpha);
    void setStipplingFillColor(glm::vec3 *fillColor);
    void setStipplingFillAlpha(float *fillAlpha);
    void setStipplingUseFillColor(bool *useFillColor);
	void setStipplingPointer(Stippling * stippling);

	//Cel Shading
	void setCelDiffuseMaterial(glm::vec3 *diffuseMaterial);
	void setCelAmbientMaterial(glm::vec3 *ambientMaterial);
	void setCelSpecularMaterial(glm::vec3 *specularMaterial);
	void setCelShininess(float *shininess);

private:
	void drawElements(GLFWwindow* window);
	std::string doubleDigitToString(double& number);

	bool m_visible;
	bool m_isoValueApplied;
	bool m_loadNewDataset;

	int m_width;
	int m_height;

	IllustrativeVisualization* m_IllustrativeVisu;

	//PERFORMANCE TAB

	//RENDERER INFO
	std::string m_renderTextA;
	std::string m_renderTextB;
	std::string m_renderTextC;

	int *m_finalFPS;
	double *m_renderTime;

	//DATA TAB

	GLint *m_isolevel;
	GLint  *m_renderMode;
	GLuint *m_VisualizationMode;
	int m_polygonMode;
	bool *m_wireframe;
	glm::vec3 *m_ClearColor;
	glm::vec3 *m_LightPosition;

	//VISUALIZATION
	//Gooch Shading
	GLuint *m_GoochSamples;

	GLfloat *m_GoochLineThickness;
	GLfloat *m_GoochSobelTreshhold;
	glm::vec3 *m_GoochDiffuseColor;
	glm::vec3 *m_GoochSpecularColor;
	glm::vec3 *m_GoochAmbientColor;

	glm::vec3 *m_GoochCoolColor;
	glm::vec3 *m_GoochWarmColor;

	GLfloat *m_GoochDiffuseCoolInfluence;
	GLfloat *m_GoochDiffuseWarmInfluence;

	//Contours Shading
	GLfloat *m_ContoursLineThickness;
	glm::vec3 *m_ContoursLineColor;
	glm::vec3 *m_ContoursSolidColor;
	GLfloat *m_ContoursSolidAlpha;
	bool *m_ContoursUseSolidColor;

	//hatching
	float *m_HatchingTheta;
	float *m_HatchingPatternSize;
	int *m_HatchingIntensity;
	glm::fvec3 *m_HatchingColor;
	float *m_HatchingAlpha;
	glm::fvec3 *m_HatchingFillColor;
	float *m_HatchingFillAlpha;
	bool *m_HatchingUseFillColor;

    //stippling
    glm::fvec3 *m_StipplingColor;
    float *m_StipplingAlpha;
    glm::fvec3 *m_StipplingFillColor;
    float *m_StipplingFillAlpha;
    bool *m_StipplingUseFillColor;
	Stippling *m_stippling;

	int m_StipplingResX;
	int m_StipplingResY;
	int m_StipplingResZ;

	//Cel Shading
	glm::vec3 *m_CelDiffuseMaterial;
	glm::vec3 *m_CelAmbientMaterial;
	glm::vec3 *m_CelSpecularMaterial;
	float *m_CelShininess;
};
