#include "DicomManager.h"

using namespace puntoexe;
using namespace std;

DicomManager::DicomManager(void){
	uiSliceCount = 0;
	uiWidth = 0;
	uiHeight = 0;

	dPixelSpacing = 0;
	dSliceSpacing = 0;
}

DicomManager::~DicomManager(void){
	clearImagaData();
}

void DicomManager::readData(string dataPath){
	ptr<stream> readStream(new stream);
	readStream->openFile(dataPath, ios::in);

	ptr<streamReader> reader(new streamReader(readStream));
	ptr<imebra::dataSet> dataSet =
		imebra::codecs::codecFactory::getCodecFactory()->load(reader);

	ptr<imebra::image> firstImage = dataSet->getImage(0);
	dPixelSpacing = dataSet->getDouble(0x0028, 0, 0x0030, 0);

	//abs taking in a double is deprecated on linux - therefore we do it manually as a temp fix
	double tempScliceSpacing = dataSet->getDouble(0x0018, 0, 0x0088, 0);
	if (tempScliceSpacing == 0.0)
		tempScliceSpacing = dataSet->getDouble(0x0018, 0, 0x0050, 0) / 2.0f;

	if (tempScliceSpacing < 0.0)
		tempScliceSpacing *= -1;
	dSliceSpacing = tempScliceSpacing;

	vPosition.push_back(glm::vec3(dataSet->getDouble(0x0020, 0, 0x0032, 0), dataSet->getDouble(0x0020, 0, 0x0032, 1), dataSet->getDouble(0x0020, 0, 0x0032, 2)));

	firstImage->getSize(&uiWidth, &uiHeight);

	ptr<imebra::transforms::transform> modVOILUT(
		new imebra::transforms::modalityVOILUT(dataSet));
	ptr<imebra::image> convertedImage(modVOILUT->allocateOutputImage(firstImage, uiWidth, uiHeight));
	modVOILUT->runTransform(firstImage, 0, 0, uiWidth, uiHeight, convertedImage, 0, 0);

	ptr<imebra::transforms::transform> myVoiLut(
		new imebra::transforms::VOILUT(dataSet));

	ptr<imebra::image> presentationImage(myVoiLut->allocateOutputImage(convertedImage, uiWidth, uiHeight));
	myVoiLut->runTransform(convertedImage, 0, 0, uiWidth, uiHeight, presentationImage, 0, 0);


	imbxUint32 rowSize, channelPixelSize, channelsNumber;
	ptr<imebra::handlers::dataHandlerNumericBase> myHandler = presentationImage->getDataHandler(false, &rowSize, &channelPixelSize, &channelsNumber);

	// Retrieve the image's size in pixels
	imbxUint32 sizeX, sizeY;
	presentationImage->getSize(&sizeX, &sizeY);

	// Scan all the rows
	imbxUint32 index(0);
	for (imbxUint32 scanY = 0; scanY < sizeY; ++scanY){
		// Scan all the columns
		for (imbxUint32 scanX = 0; scanX < sizeX; ++scanX){
			// Scan all the channels
			for (imbxUint32 scanChannel = 0; scanChannel < channelsNumber; ++scanChannel){
				iImageData.push_back(myHandler->getSignedLong(index++));
			}
		}
	}

	uiSliceCount++;
}

void DicomManager::filterImageData(int kernelSize){
	if (kernelSize % 2 == 0) kernelSize -= 1;
	std::cout << "KERNEL SIZE: " << kernelSize << std::endl;

	if (kernelSize >= 3) {
		imbxInt32 * tempData = new imbxInt32[iImageData.size()];

		vector<int> kernel;

		int diffValue = 0;
		int loopBorder = (kernelSize - 1) / 2;

		std::cout << "FILTER KERNEL: ( ";
		//Create Kernel
		for (int j = -loopBorder; j <= loopBorder; j++){
			int value = (int)pow(2, loopBorder - abs(j));
			kernel.push_back(value);
			std::cout << value << " ";;
		}
		std::cout << ")" << std::endl;

		for (int i = 0; i < kernelSize; i++) diffValue += kernel[i];
		if (diffValue == 0) diffValue = 1;

		//Filter x Direction
		for (int i = 0; i < iImageData.size(); i++){
			int x = i % uiWidth;
			int y = (i / uiWidth) % uiHeight;
			int z = (i / (uiWidth * uiHeight)) % uiSliceCount;

			int value = 0;

			for (int j = -loopBorder; j <= loopBorder; j++){
				int tempX = x;
				if (!(x + j < 0 || x + j >= uiWidth)) tempX = x + j;
				int temp = tempX + (uiWidth * y) + (uiWidth * uiHeight * z);
				value += kernel[j + loopBorder] * iImageData[temp];
			}

			tempData[i] = value / diffValue;
		}

		//Filter y Direction
		for (int i = 0; i < iImageData.size(); i++){
			int x = i % uiWidth;
			int y = (i / uiWidth) % uiHeight;
			int z = (i / (uiWidth * uiHeight)) % uiSliceCount;

			int value = 0;

			for (int j = -loopBorder; j <= loopBorder; j++){
				int tempY = y;
				if (!(y + j < 0 || y + j >= uiHeight)) tempY = y + j;
				int temp = x + (uiWidth * tempY) + (uiWidth * uiHeight * z);
				value += kernel[j + loopBorder] * tempData[temp];
			}

			iImageData[i] = value / diffValue;
		}

		//Filter z Direction
		for (int i = 0; i < iImageData.size(); i++){
			int x = i % uiWidth;
			int y = (i / uiWidth) % uiHeight;
			int z = (i / (uiWidth * uiHeight)) % uiSliceCount;

			int value = 0;

			for (int j = -loopBorder; j <= loopBorder; j++){
				int tempZ = z;
				if (!(z + j < 0 || z + j >= uiSliceCount)) tempZ = z + j;
				int temp = x + (uiWidth * y) + (uiWidth * uiHeight * tempZ);
				value += kernel[j + loopBorder] * iImageData[temp];
			}

			tempData[i] = value / diffValue;
		}

		for (int i = 0; i < iImageData.size(); i++){
			iImageData[i] = tempData[i];
		}

		delete[] tempData;
	}
}

void DicomManager::clearImagaData(){
	iImageData.clear();
	iImageData.shrink_to_fit();

	vPosition.clear();
	vPosition.shrink_to_fit();

	uiSliceCount = 0;
}

vector<imbxInt32> DicomManager::getImageData(){ return iImageData; }

imbxUint32 DicomManager::getImageWidth(){ return uiWidth; }
imbxUint32 DicomManager::getImageHeight(){ return uiHeight; }

double DicomManager::getPixelSpacing(){ return dPixelSpacing; }
double DicomManager::getSliceSpacing(){ return dSliceSpacing; }

glm::mat4 DicomManager::getModelMatrix(){
	glm::mat4 modelMatrix = glm::mat4(1.0f);

	if (uiSliceCount != 0){
		glm::mat4 rotateMatrix = glm::rotate(glm::mat4(1.0f), glm::degrees(3.14f * 1.5f), glm::vec3(1, 0, 0));

		GLfloat temp = vPosition[0].z - vPosition[uiSliceCount - 1].z;
		if (temp < 0.0f) rotateMatrix = glm::rotate(rotateMatrix, glm::degrees(3.14f), glm::vec3(0, 1, 0));

		float scaleY = static_cast<float>(uiHeight / uiWidth);

		float scaleZ = static_cast<float>(((dSliceSpacing / dPixelSpacing) * uiSliceCount) / uiWidth);

		modelMatrix = glm::scale(rotateMatrix, glm::vec3(1, scaleY, scaleZ));
	}
	 
	return modelMatrix;
}