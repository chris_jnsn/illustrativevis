﻿#include"DicomDataset.h"
#include"MarchingCubes.h"
#include"MarchingCubesGPU.h"
#include"DicomManager.h"
#include"MyOpenGL.h"

//ONLY USE THIS DEFINE, IF YOU WANT TO USE THE GPU IMPLEMENTATION OF MARCHING CUBES
//#define USE_GPU_MARCHING_CUBES

DicomDataset::DicomDataset(std::string const & datasetName, std::string const & path, unsigned int const & startSliceNumber, unsigned int const & endSliceNumber, signed int const & isovalue, unsigned int const & filterKernelSize) :
m_fileName(datasetName),
m_filePath(path),
m_sliceStartNum(startSliceNumber),
m_sliceEndNum(endSliceNumber),
m_sliceCount(endSliceNumber - startSliceNumber),
m_isovalue(isovalue),
m_filterKernelSize(filterKernelSize)
{
	loadData();
	marchTheCubes();
}

DicomDataset::~DicomDataset()
{
	delete m_marchingCubes;
	delete m_dicomManager;

	glDeleteBuffers(1, &m_meshBuffer);
	glDeleteBuffers(1, &m_normalBuffer);
	glDeleteBuffers(1, &m_uvBuffer);
}

void DicomDataset::changeIsovalue(signed int const newVal)
{
	if (newVal < -999)
	{
		std::cerr << "Error in DicomDataset::changeIsovalue - given value of " << newVal << " is to small. Minimum is -999";
		return;
	}

	else if (newVal > 3000)
	{
		std::cerr << "Error in DicomDataset::changeIsovalue - given value of " << newVal << " is to big. Maximum is 3000";
		return;
	}

	else
	{
		m_isovalue = newVal;

		int clo = clock();

		GLuint verticeNumber;

		glDeleteBuffers(1, &m_meshBuffer);
		glDeleteBuffers(1, &m_normalBuffer);
		glDeleteBuffers(1, &m_uvBuffer);

#ifdef USE_GPU_MARCHING_CUBES
		GLint version = MyOpenGL::getVersion();
		if (version < 43) {
			std::cout << "OPENGL 4.3 OR HIGHER IS REQUIRED" << std::endl;
		}
		else {
			m_marchingCubesGPU->generateMeshGPU(m_isovalue);
			verticeNumber = m_marchingCubesGPU->getVerticeCount();

			m_meshBuffer = m_marchingCubesGPU->getPositionBuffer();
			m_normalBuffer = m_marchingCubesGPU->getNormalBuffer();
			m_meshBuffer = m_marchingCubesGPU->getUVBuffer();
		}
#else
		m_marchingCubes->generateMeshCPU(m_isovalue);
		verticeNumber = m_marchingCubes->getVerticeCount();

		GLuint mcMeshSize = sizeof(glm::vec3) * verticeNumber;
		MyOpenGL::createVertexBuffer(1, &m_meshBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesPositionList())[0]);
		MyOpenGL::createVertexBuffer(1, &m_normalBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesNormalsList())[0]);
		MyOpenGL::createVertexBuffer(1, &m_uvBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesUVList())[0]);
		
#endif /*USE_GPU_MARCHING_CUBES*/
		double time = (clock() - clo) / 1000.0f;
		std::cout << "MARCHING CUBES FINISHED. ISOLEVEL: " << m_isovalue << " TRIANGLES: " << verticeNumber / 3 << std::endl;
		std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
	}

}

GLuint DicomDataset::getVerticeCount(){
#ifdef USE_GPU_MARCHING_CUBES
	GLint version = MyOpenGL::getVersion();
	if (version < 43) {
		std::cout << "OPENGL 4.3 OR HIGHER IS REQUIRED" << std::endl;
		return 0;
	}
	else {
		return m_marchingCubesGPU->getVerticeCount();
	}
#else
	return m_marchingCubes->getVerticeCount();
#endif /*USE_GPU_MARCHING_CUBES*/
}

void DicomDataset::loadData()
{
	int clo = clock();

	m_dicomManager = new DicomManager();

	std::string currentFile;

	for (unsigned int i = 1; i <= m_sliceCount; i++)
	{
		currentFile = m_filePath;

		if (i < 10)
		{
			currentFile.append("IM-0001-000");
			currentFile.append(std::to_string(i));
		}

		else if (i < 100)
		{
			currentFile.append("IM-0001-00");
			currentFile.append(std::to_string(i));
		}

		else
		{
			currentFile.append("IM-0001-0");
			currentFile.append(std::to_string(i));
		}

		currentFile.append(".dcm");
		//std::cout << "LOADING: " << currentFile << std::endl;
		m_dicomManager->readData(currentFile);
	}

	double time = (clock() - clo) / 1000.0f;
	std::cout << "IMAGE LOADED. SIZE: " << m_dicomManager->getImageData().size() << std::endl;
	std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
}

void DicomDataset::marchTheCubes() {

	glm::ivec3 gridSize = glm::ivec3(m_dicomManager->getImageWidth(), m_dicomManager->getImageHeight(), m_sliceCount);

	int clo = clock();

	glDeleteBuffers(1, &m_meshBuffer);
	glDeleteBuffers(1, &m_normalBuffer);
	glDeleteBuffers(1, &m_uvBuffer);
 
	m_imageData.clear();
	m_imageData.shrink_to_fit();

	float time;

#ifdef USE_GPU_MARCHING_CUBES
	GLint version = MyOpenGL::getVersion();
	if (version < 43) {
		std::cout << "OPENGL 4.3 OR HIGHER IS REQUIRED" << std::endl;
	}
	else {
		m_imageData = m_dicomManager->getImageData();

		//-------------- Marching Cubes GPU --------------
		m_marchingCubesGPU = new MarchingCubesGPU(gridSize, &m_imageData);

		time = (clock() - clo) / 1000.0f;
		std::cout << "DATA LOADED TO GPU" << std::endl;
		std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
		clo = clock();

		//Filter Image
		m_marchingCubesGPU->filterImage(m_filterKernelSize);
		std::cout << "IMAGE FILTER FINISHED." << std::endl;
		time = (clock() - clo) / 1000.0f;
		std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
		clo = clock();

		//Generate Mesh
		m_marchingCubesGPU->generateMeshGPU(m_isovalue);
		m_meshBuffer = m_marchingCubesGPU->getPositionBuffer();
		m_normalBuffer = m_marchingCubesGPU->getNormalBuffer();
		m_uvBuffer = m_marchingCubesGPU->getUVBuffer();

		std::cout << "MARCHING CUBES FINISHED. TRIANGLES: " << m_marchingCubesGPU->getVerticeCount() / 3 << std::endl;
		time = (clock() - clo) / 1000.0f;
		std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
		std::cout << std::endl;
	}
#else
	//Filter Image
	if (m_filterKernelSize >= 3){
		m_dicomManager->filterImageData(m_filterKernelSize);
		std::cout << "IMAGE FILTER FINISHED." << std::endl;
		time = (clock() - clo) / 1000.0f;
		std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
		clo = clock();
	}

	m_imageData = m_dicomManager->getImageData();

	//-------------- Marching Cubes CPU --------------
	m_marchingCubes = new MarchingCubes(gridSize, &m_imageData);
	m_marchingCubes->generateMeshCPU(m_isovalue);

	GLuint mcMeshSize = sizeof(glm::vec3) * m_marchingCubes->getVerticeCount();
	MyOpenGL::createVertexBuffer(1, &m_meshBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesPositionList())[0]);
	MyOpenGL::createVertexBuffer(1, &m_normalBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesNormalsList())[0]);
	MyOpenGL::createVertexBuffer(1, &m_uvBuffer, mcMeshSize, &(m_marchingCubes->getTrianglesUVList())[0]);
	std::cout << "MARCHING CUBES FINISHED. TRIANGLES: " << m_marchingCubes->getVerticeCount() / 3 << std::endl;

	time = (clock() - clo) / 1000.0f;
	std::cout << "TIME REQUIRED: " << time << " s" << std::endl;
	std::cout << std::endl;
#endif /*USE_GPU_MARCHING_CUBES*/

}

