#ifndef CAMERA_H
#define CAMERA_H

//--------begin Include--------
#include "MyResources.h"
//--------end Include--------

class Camera
{
public:
    /*************************************************************************
    * public methods
    *************************************************************************/
    Camera(void);
    ~Camera(void);

    void setProjection(GLfloat fovy, GLfloat aspect, GLfloat _near, GLfloat _far);
    void setCamera(glm::vec3 camPos, glm::vec3 camLookAt, glm::vec3 upVector);

    //Projection set functions
    void setFovy(GLfloat fovy);
    void setAspectRatio(GLfloat aspect);
    void setNear(GLfloat _near);
    void setFar(GLfloat _far);

    //Projection get functions
    GLfloat getFovy();
    GLfloat getAspectRatio();
    GLfloat getNear();
    GLfloat getFar();

    //Camera set functions
    void setCameraPos(glm::vec3 camPos);
    void setLookAt(glm::vec3 looktAt);
    void setUpVector(glm::vec3 upVector);

    //Camera get functions
    glm::vec3 getCameraPos();
    glm::vec3 getLookAt();
    glm::vec3 getUpVector();

    //Camera move functions
    void moveCameraPos(glm::vec3 camPos);
    void moveLookAt(glm::vec3 camLookAt);
    void setMoveValue(GLfloat value);

    void moveCameraForward();
    void moveCameraBack();
    void moveCameraLeft();
    void moveCameraRight();

    void moveLookAtAxisX(GLint speed);
    void moveLookAtAxisY(GLint speed);

    //Move around Look At
	void moveCameraSphereLeft();
	void moveCameraSphereRight();
	void moveCameraSphereUp();
	void moveCameraSphereDown();
    void moveCameraSphereForward();
    void moveCameraSphereBack();

	//Move around Look At Mouse
	void rotateCamera(GLfloat yaw, GLfloat pitch);
	void zoomCamera(GLfloat value);

    //Get Matrix
    glm::mat4 getViewMat();
    glm::mat4 getProjMat();

private:
    /*************************************************************************
    * private methods
    *************************************************************************/
    void updateMatrix();

    /*************************************************************************
    * private variables
    *************************************************************************/
    glm::mat4 c_ViewMat;
    glm::mat4 c_ProjMat;

    GLfloat c_MoveValue;

    //Camera
    glm::vec3 c_CamPosition;
    glm::vec3 c_LookAt;
    glm::vec3 c_UpVector;

    //Projection
    GLfloat c_Fovy;
    GLfloat c_AspectRatio;
    GLfloat c_Near;
    GLfloat c_Far;
};

#endif
