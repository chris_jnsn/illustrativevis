#include "Camera.h"

using namespace glm;

/*************************************************************************
* public methods
*************************************************************************/
Camera::Camera(void){
    c_CamPosition = vec3(0.0f, 2.0f, 3.0f);
    c_LookAt = vec3(0.0f, 0.0f, 0.0f);
    c_UpVector = vec3(0.0f, 1.0f, 0.0f);

    c_Fovy = 60.0f;
    c_AspectRatio = 4.0f / 3.0f;
    c_Near = 0.1f;
    c_Far = 100.f;

    c_MoveValue = 0.05f;

    updateMatrix();
}

Camera::~Camera(void){
}

void Camera::setProjection(GLfloat fovy, GLfloat aspect, GLfloat _near, GLfloat _far){
    c_Fovy = fovy;
    c_AspectRatio = aspect;
    c_Near = _near;
    c_Far = _far;

    updateMatrix();
}

void Camera::setCamera(vec3 camPos, vec3 camLookAt, vec3 upVector){
    c_CamPosition = camPos;
    c_LookAt = camLookAt;
    c_UpVector = upVector;

    updateMatrix();
}

//Projection set functions
void Camera::setFovy(GLfloat fovy){
    c_Fovy = fovy;
    updateMatrix();
}

void Camera::setAspectRatio(GLfloat aspect){
    c_AspectRatio = aspect;
    updateMatrix();
}

void Camera::setNear(GLfloat _near){
    c_Near = _near;
    updateMatrix();
}

void Camera::setFar(GLfloat _far){
    c_Far = _far;
    updateMatrix();
}

//Projection get functions
GLfloat Camera::getFovy(){
    return c_Fovy;
}

GLfloat Camera::getAspectRatio(){
    return c_AspectRatio;
}

GLfloat Camera::getNear(){
    return c_Near;
}

GLfloat Camera::getFar(){
    return c_Far;
}

//Camera set functions
void Camera::setCameraPos(vec3 camPos){
    c_CamPosition = camPos;

    updateMatrix();
}

void Camera::setLookAt(vec3 camLookAt){
    c_LookAt = camLookAt;
    updateMatrix();
}

void Camera::setUpVector(vec3 upVector){
    c_UpVector = normalize(upVector);
    updateMatrix();
}

//Camera get functions
vec3 Camera::getCameraPos(){
    return c_CamPosition;
}

vec3 Camera::getLookAt(){
    return c_LookAt;
}

vec3 Camera::getUpVector(){
    return c_UpVector;
}

//Camera move functions
void Camera::moveCameraPos(vec3 camPos){
    c_CamPosition = c_CamPosition + camPos;
    updateMatrix();
}

void Camera::moveLookAt(vec3 camLookAt){
    c_LookAt = c_LookAt + camLookAt;
    updateMatrix();
}

void Camera::setMoveValue(GLfloat value){
    c_MoveValue = value;
}

//Move Camera
void Camera::moveCameraForward(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition) * c_MoveValue;

    c_CamPosition = c_CamPosition + moveStep;
    c_LookAt = c_LookAt + moveStep;

    updateMatrix();
}

void Camera::moveCameraBack(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition) * c_MoveValue;

    c_CamPosition = c_CamPosition - moveStep;
    c_LookAt = c_LookAt - moveStep;

    updateMatrix();
}

void Camera::moveCameraLeft(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
    moveStep = normalize(cross(c_UpVector, moveStep)) * c_MoveValue;

    c_CamPosition = c_CamPosition + moveStep;
    c_LookAt = c_LookAt + moveStep;

    updateMatrix();
}

void Camera::moveCameraRight(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
    moveStep = normalize(cross(c_UpVector, moveStep)) * c_MoveValue;

    c_CamPosition = c_CamPosition - moveStep;
    c_LookAt = c_LookAt - moveStep;

    updateMatrix();
}

//Move LookAt
void Camera::moveLookAtAxisX(GLint speed){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
    moveStep = normalize(cross(c_UpVector, moveStep)) * (1.0f * speed);

    c_LookAt = c_LookAt - moveStep * 0.01f;

    updateMatrix();
}

void Camera::moveLookAtAxisY(GLint speed){
    vec3 temp = c_LookAt;
    GLfloat distance = dot(normalize(c_LookAt - c_CamPosition), c_UpVector);

    vec3 sideVector = normalize(c_LookAt - c_CamPosition);
    sideVector = normalize(cross(c_UpVector, sideVector));

    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
    moveStep = normalize(cross(moveStep, sideVector)) * (1.0f * speed);

    c_LookAt = c_LookAt - moveStep * 0.01f;

    if(distance < -0.99f || distance > 0.99f) c_LookAt = temp;

    updateMatrix();
}

//Move around Look At
void Camera::moveCameraSphereLeft(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
	moveStep = normalize(cross(c_UpVector, moveStep)) * c_MoveValue;

    c_CamPosition = c_CamPosition + moveStep;

    updateMatrix();
}

void Camera::moveCameraSphereRight(){
    vec3 moveStep = normalize(c_LookAt - c_CamPosition);
	moveStep = normalize(cross(c_UpVector, moveStep)) * c_MoveValue;

    c_CamPosition = c_CamPosition - moveStep;

    updateMatrix();
}

void Camera::moveCameraSphereUp(){
    GLfloat distance = dot(normalize(c_LookAt - c_CamPosition), c_UpVector);
    if(distance > -0.99f){
        vec3 moveStep = normalize(c_LookAt - c_CamPosition);
		moveStep = normalize(cross(normalize(cross(c_UpVector, moveStep)), moveStep)) * c_MoveValue;

        c_CamPosition = c_CamPosition - moveStep;

        updateMatrix();
    }
}

void Camera::moveCameraSphereDown(){
    GLfloat distance = dot(normalize(c_LookAt - c_CamPosition), c_UpVector);
    if(distance < 0.99f){
        vec3 moveStep = normalize(c_LookAt - c_CamPosition);
		moveStep = normalize(cross(normalize(cross(c_UpVector, moveStep)), moveStep)) * c_MoveValue;

        c_CamPosition = c_CamPosition + moveStep;

        updateMatrix();
    }
}

void Camera::moveCameraSphereForward(){
    GLfloat distance = dot(c_LookAt - c_CamPosition, c_LookAt - c_CamPosition);
    if(distance > 0.1f){
        vec3 moveStep = normalize(c_LookAt - c_CamPosition) * c_MoveValue;

        c_CamPosition = c_CamPosition + moveStep;

        updateMatrix();
    }
}

void Camera::moveCameraSphereBack(){
    GLfloat distance = dot(c_LookAt - c_CamPosition, c_LookAt - c_CamPosition);
    if(distance < 100.0f){
        vec3 moveStep = normalize(c_LookAt - c_CamPosition) * c_MoveValue;

        c_CamPosition = c_CamPosition - moveStep;

        updateMatrix();
    }
}

//Move around Look At Mouse
void Camera::rotateCamera(GLfloat yaw, GLfloat pitch){
	GLfloat maxAngle = 5.0f;

	if (yaw > maxAngle) yaw = maxAngle;
	else if (yaw < -maxAngle) yaw = -maxAngle;
	if (pitch > maxAngle) pitch = maxAngle;
	else if (pitch < -maxAngle) pitch = -maxAngle;
		
	vec3 direction = normalize(c_CamPosition - c_LookAt);
	GLfloat distance = length(c_CamPosition - c_LookAt);

	mat4 rotateYaw = rotate(mat4(1.0f), yaw, c_UpVector);
	
	vec3 right = normalize(cross(c_UpVector, direction));

	//prevent gimbal lock
	GLfloat angle = degrees(acos(dot(c_UpVector, direction) / (length(c_UpVector) * length(direction))));
	if ((angle + pitch) >= 180.0f) pitch -= (angle + pitch) - 180.0f;
	else if ((angle + pitch) <= 0.0f) pitch -= (angle + pitch);

	mat4 rotatePitch = rotate(mat4(1.0f), pitch, right);

	vec4 newDir = rotateYaw * rotatePitch * normalize(vec4(direction, 1));

	c_CamPosition = normalize(vec3(newDir.x, newDir.y, newDir.z)) * distance;

	updateMatrix();
}

void Camera::zoomCamera(GLfloat value){
	vec3 direction = normalize(c_CamPosition - c_LookAt);
	GLfloat distance = length(c_CamPosition - c_LookAt);

	GLfloat maxDistance = 100.0f;
	GLfloat minDistance = 0.1f;

	if ((distance + value) >= maxDistance) distance -= (distance + value) - maxDistance;
	else if ((distance + value) <= minDistance) distance -= (distance + value) - minDistance;

	distance += value;
	c_CamPosition = direction * distance;

	updateMatrix();
}

//Get Matrix
mat4 Camera::getViewMat(){
    return c_ViewMat;
}

mat4 Camera::getProjMat(){
    return c_ProjMat;
}

/*************************************************************************
* private methods
*************************************************************************/
void Camera::updateMatrix(){
    c_ProjMat = perspective(c_Fovy, c_AspectRatio, c_Near, c_Far);
    c_ViewMat = lookAt(c_CamPosition, c_LookAt, c_UpVector);
}
