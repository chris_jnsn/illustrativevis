#ifndef NORMALSHADING_H
#define NORMALSHADING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class SimpleShading
{
public:
	SimpleShading();
	~SimpleShading();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

	void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	void setLightPosition(glm::vec3 *lightPosition);
	
	GLint uiRenderMode;

private:
	/*************************************************************************
	* private methods
	*************************************************************************/


	/*************************************************************************
	* private variables
	*************************************************************************/
	//General Settings
	glm::vec3 *vLightPosition;
	GLuint uiWindowWidth;
	GLuint uiWindowHeight;

	//Shader Programs
	GLuint renderProgram;

	//Mesh Data
	GLuint uiMeshBuffer;
	GLuint uiNormalBuffer;
	GLuint uiVertexCount;
	glm::mat4 mModelMatrix;

};

#endif // NORMALSHADING_H