#ifndef MARCHINGCUBESGPU_H
#define MARCHINGCUBESGPU_H

#include "MyResources.h"
#include "MyOpenGL.h"

class MarchingCubesGPU
{
public:
    MarchingCubesGPU(glm::ivec3 gridSize, std::vector<GLint>* volumeData);

	void filterImage(GLint kernelSize);
    void generateMeshGPU(GLint isolevel);

    GLuint & getPositionBuffer();
    GLuint & getNormalBuffer();
    GLuint & getUVBuffer();
    GLuint getVerticeCount();

private:
    /*************************************************************************
    * private methods
    *************************************************************************/
	void generateMeshTransformFeedback();

    /*************************************************************************
    * private variables
    *************************************************************************/
    std::vector<GLint>* iVolumeData;

    glm::ivec3 ivGridSize;
    GLint iIsolevel;

    GLuint uiTFPositionBuffers;
    GLuint uiTFNormalBuffers;
    GLuint uiVerticeCount;

	//Shader Programs
	GLuint uiMarchingCubesShader;
	GLuint uiBinomialFilter;
	GLuint uiCellValueCompute;

	//Textures
	GLuint uiTriTableTexture;
	GLuint uiDataTexture;
};

#endif // MARCHINGCUBESGPU_H
