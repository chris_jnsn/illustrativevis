#ifndef STIPPLING_H
#define STIPPLING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class Stippling
{
public:
    Stippling();
    ~Stippling();

    void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint uvBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

    void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);

    void setLightPosition(glm::vec3 *lightPosition);
    void createNoiseTexture(unsigned int w, unsigned int h, unsigned int d);

    glm::fvec3 m_stipplingColor;
    float m_stipplingAlpha;
    glm::fvec3 m_fillColor;
    float m_fillAlpha;

    bool m_useFillColor;

private:

    /*************************************************************************
    * private variables
    *************************************************************************/
    glm::vec3 *vLightPosition;

    //General Settings
    GLuint uiWindowWidth;
    GLuint uiWindowHeight;

    //Mesh Data
    GLuint uiMeshBuffer;
    GLuint uiNormalBuffer;
    GLuint uiUVBuffer;
    GLuint uiVertexCount;
    glm::mat4 mModelMatrix;

    //Stippling Parameters
    GLuint m_noiseTexture;

    //Shader Programs
    GLuint renderProgram;
};

#endif // STIPPLING_H