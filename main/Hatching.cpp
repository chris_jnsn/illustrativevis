#include "Hatching.h"

using namespace glm;

Hatching::Hatching(){
    uiMeshBuffer = 0;
    uiNormalBuffer = 0;
    uiVertexCount = 0;
    mModelMatrix = mat4(1.0f);

    //Parameter
    m_theta = 9.868;
    m_patternSize = 1.0f;
    m_intensity = 1;
    m_hatchingColor = glm::fvec3(0.0f,0.0f,0.0f);
    m_hatchingAlpha = 1.0f;
    m_fillColor = glm::fvec3(1.0f,1.0f,1.0f);
    m_fillAlpha = 1.0f;
    m_useFillColor = false;

    uiWindowWidth = 0;
    uiWindowHeight = 0;
}

Hatching::~Hatching(){

}

void Hatching::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
    uiMeshBuffer = meshBuffer;
    uiNormalBuffer = normalBuffer;
    uiVertexCount = vertexCount;
    mModelMatrix = modelMatrix;
	renderProgram = program;
}

void Hatching::setLightPosition(glm::vec3 *lightPosition){
    vLightPosition = lightPosition;
}

void Hatching::render(Camera* camera, GLuint windowWidth, GLuint windowHeight)
{

    if (uiWindowWidth != windowWidth || uiWindowHeight != windowHeight)
    {
        uiWindowWidth = windowWidth;
        uiWindowHeight = windowHeight;
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLuint program = renderProgram;

    //Draw to Texture
    glEnable(GL_DEPTH_TEST);
	
    glUseProgram(program);

    //Bind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //HATCHING UNIFORMS==========================================================================================
    glUniform4f(glGetUniformLocation(program, "lightPosition"), vLightPosition->x, vLightPosition->y, vLightPosition->z, 1.0f);

    //Rotation Matrix created once on cpu and send to the shader
    glm::mat2 hatchingThetaRotation = glm::mat2(
            glm::cos(m_theta),
            -glm::sin(m_theta),
            glm::sin(m_theta),
            glm::cos(m_theta)
    );

	glUniform1i(glGetUniformLocation(program, "useHatching"), true);

    glUniformMatrix2fv(glGetUniformLocation(program, "thetaRotation"),1, GL_FALSE,glm::value_ptr(hatchingThetaRotation));
    glUniform1i(glGetUniformLocation(program, "patternSize"), m_patternSize);
    glUniform1f(glGetUniformLocation(program, "minIntensity"), m_intensity);

    glm::fvec4 hatchingC = glm::fvec4(m_hatchingColor.x,m_hatchingColor.y,m_hatchingColor.z,m_hatchingAlpha);
    glUniform4fv(glGetUniformLocation(program,"hatchingColor"),1,glm::value_ptr(hatchingC));


    glm::fvec4 fillC;
    if(m_useFillColor)
        fillC = glm::fvec4(m_fillColor.x,m_fillColor.y,m_fillColor.z,m_fillAlpha);
    else
        fillC = glm::fvec4(m_fillColor.x,m_fillColor.y,m_fillColor.z,0.0);

    glUniform4fv(glGetUniformLocation(program,"fillColor"),1,glm::value_ptr(fillC));
    //===========================================================================================================

    //Set Transform Matrices
    MyOpenGL::setMatrices(program, mModelMatrix, camera);

    glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);	

	glUniform1i(glGetUniformLocation(program, "useHatching"), false);
}