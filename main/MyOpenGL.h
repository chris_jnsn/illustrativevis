﻿#ifndef MYOPENGL_H
#define MYOPENGL_H

#include "MyResources.h"
#include "Camera.h"

namespace MyOpenGL {
    struct SHADER {
        SHADER(void){
        }
        SHADER(std::string source, GLuint type){
            this->source = source;
            this->type = type;
        }
        std::string source;
        GLuint type;
    };

	static GLint getVersion() {
		int glVersion[2] = { 0, 0 }; 
		glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
		glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
		GLint value = (glVersion[0] * 10) + glVersion[0];
		return value;
	}

    static void checkShader(GLuint shaderHandle) {
        GLint status;
        glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &status);

        if (status == GL_FALSE) {
            GLint infoLogLength;
            glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &infoLogLength);

            GLchar* infoLog = new GLchar[infoLogLength + 1];
            glGetShaderInfoLog(shaderHandle, infoLogLength, NULL, infoLog);

            std::cout << "ERROR: Unable to compile shader" << std::endl << infoLog << std::endl;
            delete[] infoLog;
        } else {
            std::cout << "SUCCESS: Shader compiled" << std::endl;
        }
    }

    static void loadShaderSource(int shaderHandle, const char* fileName) {
        std::string fileContent;
        std::string line;

        //open file and "parse" input
        std::ifstream file(fileName);
        if (file.is_open()) {
            while (!file.eof()){
                getline (file, line);
                fileContent += line + "\n";
            }
            file.close();
            std::cout << "SUCCESS: Opened file " << fileName << std::endl;
        }
        else
            std::cout << "ERROR: Unable to open file " << fileName << std::endl;

        const char* source = fileContent.c_str();
        const GLint source_size = strlen(source);

        glShaderSource(shaderHandle, 1, &source, &source_size);
    }

    static GLuint createProgram(SHADER shader){
        GLuint programHandle = glCreateProgram();

        GLuint shaderHandle = glCreateShader(shader.type);
        loadShaderSource(shaderHandle, shader.source.c_str());
        glCompileShader(shaderHandle);
        checkShader(shaderHandle);
        glAttachShader(programHandle, shaderHandle);
        std::cout << std::endl;

        glLinkProgram(programHandle);

        glDeleteShader(shaderHandle);

        return programHandle;
    }

    static GLuint createProgram(SHADER* shader, GLuint count){
        GLuint programHandle = glCreateProgram();
        GLuint* shaderHandle = new GLuint[count];

        for(unsigned int i = 0; i < count; i++){
            shaderHandle[i] = glCreateShader(shader[i].type);
            loadShaderSource(shaderHandle[i], shader[i].source.c_str());
            glCompileShader(shaderHandle[i]);
            checkShader(shaderHandle[i]);
            glAttachShader(programHandle, shaderHandle[i]);
            std::cout << std::endl;
        }
        glLinkProgram(programHandle);

        for (unsigned int i = 0; i < count; i++){
            glDeleteShader(shaderHandle[i]);
        }

        return programHandle;
    }

    //Textures
    static void create2DTexture(GLenum index, GLuint* texture, GLint filterParam, GLint wrapParam, GLint level, GLint internalFormat,
                                        GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid* data){
        glGenTextures(1, texture);
        glActiveTexture(index);
        glEnable(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, *texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterParam);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterParam);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapParam);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapParam);

        glTexImage2D( GL_TEXTURE_2D, level, internalFormat, width, height, border, format, type, data);
    }

    static void create3DTexture(GLenum index, GLuint* texture, GLint filterParam, GLint wrapParam, GLint level, GLint internalFormat,
                                        GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid* data){
        glGenTextures(1, texture);
        glActiveTexture(index);
        glEnable(GL_TEXTURE_3D);

        glBindTexture(GL_TEXTURE_3D, *texture);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, filterParam);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, filterParam);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, wrapParam);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, wrapParam);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, wrapParam);

        glTexImage3D( GL_TEXTURE_3D, level, internalFormat, width, height, depth, border, format, type, data);
    }

    //Buffer
    static void createVertexBuffer(GLsizei n, GLuint* buffer, GLsizeiptr size, const GLvoid* data){
        glGenBuffers( n, buffer);
        glBindBuffer( GL_ARRAY_BUFFER, *buffer );
        glBufferData( GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW );
    }

    static void createStorageBuffer(GLsizei n, GLuint* buffer, GLsizeiptr size, const GLvoid* data, GLuint index){
        glGenBuffers( n, buffer);
        glBindBuffer( GL_SHADER_STORAGE_BUFFER, *buffer );
        glBufferData( GL_SHADER_STORAGE_BUFFER, size , data, GL_STATIC_DRAW );
        glBindBufferBase( GL_SHADER_STORAGE_BUFFER, index, *buffer );
    }

	static void setMatrices(GLuint shaderProgram, glm::mat4 modelMatrix, Camera* camera){

		glm::mat4 normalMatrix = glm::transpose(glm::inverse(camera->getViewMat() * modelMatrix));

		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "uniformModel"), 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "uniformView"), 1, GL_FALSE, glm::value_ptr(camera->getViewMat()));
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "uniformProjection"), 1, GL_FALSE, glm::value_ptr(camera->getProjMat()));
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "uniformNormal"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}

}

#endif // MYOPENGL_H
