#ifndef MYRESOURCES
#define MYRESOURCES

/*************************************************************************
 * Includes
 *************************************************************************/
//OpenGL
//#define GLEW_STATIC
#include <GL/glew.h>

#include <GLFW/glfw3.h>

//String
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

//glm & math
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <math.h>
#include <vector>

/*************************************************************************
* Enumerations
*************************************************************************/
namespace MyEnums {
	enum VisualizationMode { SIMPLE_SHADING = 0, ILLUSTRATIVE_VISUALIZATION, SHADINGMODE_COUNT };
}

#endif // MYRESOURCES