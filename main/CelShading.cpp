#include "CelShading.h"

using namespace glm;

CelShading::CelShading(){
	uiMeshBuffer = 0;
	uiNormalBuffer = 0;
	uiVertexCount = 0;
	mModelMatrix = mat4(1.0f);

	vLightPosition = new vec3(2.0f, 2.0f, 2.0f);
	m_diffuseMaterial = glm::vec3(1.0f, 1.0f, 1.0f);
	m_ambientMaterial = glm::vec3(0.0f, 0.0f, 1.0f);
	m_specularMaterial = glm::vec3(0.0f, 0.5f, 0.0f);
	m_shininess = 0.9f;

	uiWindowWidth = 0;
	uiWindowHeight = 0;
}

CelShading::~CelShading(){

}

void CelShading::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
	uiMeshBuffer = meshBuffer;
	uiNormalBuffer = normalBuffer;
	uiVertexCount = vertexCount;
	mModelMatrix = modelMatrix;
	renderProgram = program;
}

void CelShading::render(Camera* camera, GLuint windowWidth, GLuint windowHeight){

	if (uiWindowWidth != windowWidth || uiWindowHeight != windowHeight)
	{
		uiWindowWidth = windowWidth;
		uiWindowHeight = windowHeight;
	}

	glEnable(GL_DEPTH_TEST);

	GLuint program = renderProgram;
	glUseProgram(program);

	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glUniform1i(glGetUniformLocation(program, "useCel"), true);

	glUniform3f(glGetUniformLocation(program, "LightPosition"), vLightPosition->x, vLightPosition->y, vLightPosition->z);
	glUniform3f(glGetUniformLocation(program, "DiffuseMaterial"), m_diffuseMaterial.x, m_diffuseMaterial.y, m_diffuseMaterial.z);
	glUniform3f(glGetUniformLocation(program, "AmbientMaterial"), m_ambientMaterial.x, m_ambientMaterial.y, m_ambientMaterial.z);
	glUniform3f(glGetUniformLocation(program, "SpecularMaterial"), m_specularMaterial.x, m_specularMaterial.y, m_specularMaterial.z);
	glUniform1f(glGetUniformLocation(program, "Shininess"), m_shininess);

	MyOpenGL::setMatrices(program, mModelMatrix, camera);

	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDisable(GL_DEPTH_TEST);

	glUniform1i(glGetUniformLocation(program, "useCel"), false);
}

void CelShading::setLightPosition(glm::vec3 *lightPosition){
	vLightPosition = lightPosition;
}