#include "ContoursShading.h"

using namespace glm;

ContoursShading::ContoursShading(){
	uiMeshBuffer = 0;
	uiNormalBuffer = 0;
	uiVertexCount = 0;
	mModelMatrix = mat4(1.0f);

	//Parameter
	fLineThickness = 2.0f;
	vLineColor = vec3(0.0f, 0.0f, 0.0f);
	vSolidColor = vec3(1.0f, 1.0f, 1.0f);
	fSolidAlpha = 1.0f;
	useSolidColor = false;

	uiWindowWidth = 0;
	uiWindowHeight = 0;

	//Render to Texture Program
	MyOpenGL::SHADER SHADER_TEXTURE_VERTEX(SHADERS_PATH "/Visualisation/VisualisationVertex.glsl", GL_VERTEX_SHADER);
	MyOpenGL::SHADER SHADER_TEXTURE_GEOMETRY(SHADERS_PATH "/Contours/ContoursMainGeometry.glsl", GL_GEOMETRY_SHADER);
	MyOpenGL::SHADER SHADER_TEXTURE_FRAGMENT(SHADERS_PATH "/Visualisation/VisualisationFragment.glsl", GL_FRAGMENT_SHADER);

	MyOpenGL::SHADER textureShader[3];
	textureShader[0] = SHADER_TEXTURE_VERTEX;
	textureShader[1] = SHADER_TEXTURE_GEOMETRY;
	textureShader[2] = SHADER_TEXTURE_FRAGMENT;
	uiContoursProgram = MyOpenGL::createProgram(textureShader, 3);
}

ContoursShading::~ContoursShading(){

}

void ContoursShading::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
	uiMeshBuffer = meshBuffer;
	uiNormalBuffer = normalBuffer;
	uiVertexCount = vertexCount;
	mModelMatrix = modelMatrix;
	uiSolidShadingProgram = program;
}

void ContoursShading::render(Camera* camera, GLuint windowWidth, GLuint windowHeight){	
	if (uiWindowWidth != windowWidth || uiWindowHeight != windowHeight){
		uiWindowWidth = windowWidth;
		uiWindowHeight = windowHeight;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	renderContours(camera, windowWidth, windowHeight);
	renderSolidObject(camera, windowWidth, windowHeight);
}

void ContoursShading::renderSolidObject(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_FALSE);

	GLuint program = uiSolidShadingProgram;

	//Draw to Texture
	glEnable(GL_DEPTH_TEST);

	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Set Uniforms
	glUniform1i(glGetUniformLocation(program, "useContoursSolid"), GL_TRUE);

	if(useSolidColor)
		glUniform4f(glGetUniformLocation(program, "solidColor"), vSolidColor.x, vSolidColor.y, vSolidColor.z, fSolidAlpha);
	else
		glUniform4f(glGetUniformLocation(program, "solidColor"), vSolidColor.x, vSolidColor.y, vSolidColor.z, 0.0f);

	//Set Transform Matrices
	MyOpenGL::setMatrices(program, mModelMatrix, camera);

	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDepthMask(GL_TRUE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	glUniform1i(glGetUniformLocation(program, "useContoursSolid"), GL_FALSE);
}

void ContoursShading::renderContours(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = uiContoursProgram;

	//Draw to Texture
	glEnable(GL_DEPTH_TEST);

	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Set Uniforms
	glUniform1i(glGetUniformLocation(program, "useContoursMain"), GL_TRUE);

	glUniform3f(glGetUniformLocation(program, "lineColor"), vLineColor.x, vLineColor.y, vLineColor.z);

	//Set Transform Matrices
	MyOpenGL::setMatrices(program, mModelMatrix, camera);
	
	glLineWidth(fLineThickness);
	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDisable(GL_DEPTH_TEST);
	glLineWidth(1.0f);

	glUniform1i(glGetUniformLocation(program, "useContoursMain"), GL_FALSE);
}

void ContoursShading::fillDepthBuffer(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = uiSolidShadingProgram;

	//Disable Color Draw
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	//Draw to Texture
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Set Uniforms
	glUniform1i(glGetUniformLocation(program, "useContoursSolid"), GL_TRUE);

	glUniform4f(glGetUniformLocation(program, "solidColor"), 1.0f, 1.0f, 1.0f, 1.0f);

	//Set Transform Matrices
	MyOpenGL::setMatrices(program, mModelMatrix, camera);

	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);

	glDisable(GL_DEPTH_TEST);

	//Enable Color Draw
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	glUniform1i(glGetUniformLocation(program, "useContoursSolid"), GL_FALSE);
}