#ifndef ILLUSTRATIVEVISUALIZATION_H
#define ILLUSTRATIVEVISUALIZATION_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

#include "SimpleShading.h"
#include "GoochShading.h"
#include "ContoursShading.h"
#include "Hatching.h"
#include "CelShading.h"
#include "Stippling.h"

class IllustrativeVisualization
{
public:
	IllustrativeVisualization();
	~IllustrativeVisualization();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint uvBuffer, GLuint vertexCount, glm::mat4 modelMatrix);
	void render(Camera* camera, GLuint windowWidth, GLuint windowHeight, GLuint mode);

	SimpleShading* getSimpleShading() { return simpleShading; }
	GoochShading* getGoochShading() { return goochShading; }
	ContoursShading* getContoursShading() { return contoursShading; }
	Hatching* getHatching() { return hatching; }
	CelShading* getCelShading() { return celShading; }
	Stippling* getStippling() { return stippling; }

	static bool USE_GOOCH_SHADING;
	static bool USE_CONTOURS_SHADING;
	static bool USE_HATCHING;
	static bool USE_CELSHADING;
	static bool USE_STIPPLING;
 
	glm::vec3 vLightPosition;

private:
	void renderSimpleShading(Camera* camera, GLuint windowWidth, GLuint windowHeight);
	void renderIllustrative(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	SimpleShading* simpleShading;
	GoochShading* goochShading;
	ContoursShading* contoursShading;
	Hatching* hatching;
	CelShading* celShading;
	Stippling* stippling;

	GLuint uiNormalRenderProgram;
};

#endif // ILLUSTRATIVEVISUALIZATION_H