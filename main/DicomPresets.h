#pragma once
#include<vector>
#include<string>

struct DicomPreset
{
    DicomPreset(std::string fileName, std::string filePath, int numOfSlices)
    {
        this->filePath = filePath;
        this->fileName = fileName;
        this->numOfSlices = numOfSlices;
    };

    std::string filePath;
    std::string fileName;
    int numOfSlices;
};

//README
//For each Dataset that should be loadable do the following:
//Add one entry to the "dicomPresets.txt" file in the ./Resources/DICOM Folder

class DicomPresets
{
public:

    static std::vector<DicomPreset*> DICOM_PRESETS;
    static int NUMBER_OF_PRESETS;
    static void loadPresetsFromFile();
private:
};
