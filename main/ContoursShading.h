#ifndef CONTOURSSHADING_H
#define CONTOURSSHADING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class ContoursShading
{
public:
	ContoursShading();
	~ContoursShading();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

	void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);
	void fillDepthBuffer(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	//Parameter
	GLfloat fLineThickness;
	glm::vec3 vLineColor;
	glm::vec3 vSolidColor;
	bool useSolidColor;
	GLfloat fSolidAlpha;

private:
	/*************************************************************************
	* private methods
	*************************************************************************/
	void renderSolidObject(Camera* camera, GLuint windowWidth, GLuint windowHeight);
	void renderContours(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	/*************************************************************************
	* private variables
	*************************************************************************/
	//General Settings
	GLuint uiWindowWidth;
	GLuint uiWindowHeight;

	//Mesh Data
	GLuint uiMeshBuffer;
	GLuint uiNormalBuffer;
	GLuint uiVertexCount;
	glm::mat4 mModelMatrix;

	//Shader Programs
	GLuint uiContoursProgram;
	GLuint uiSolidShadingProgram;
};

#endif // CONTOURSSHADING_H