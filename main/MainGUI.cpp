#include "MainGUI.h"
#include <imgui.h>
#include "IllustrativeVisualization.h"

static bool         mousePressed[2] = { false, false };

const ImVec4 qualityGreen = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);           //quality measurement colors for FPS etc..
const ImVec4 qualityYellow = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
const ImVec4 qualityRed = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);

MainGUI::MainGUI() :
m_visible(true),
m_isoValueApplied(false),
m_loadNewDataset(false),
m_renderTextA(std::string()), m_renderTextB(std::string()), m_renderTextC(std::string()),
m_wireframe(nullptr),
m_finalFPS(nullptr),
m_isolevel(nullptr),
m_width(280),
m_height(800),
m_StipplingResX(300),
m_StipplingResY(300),
m_StipplingResZ(300)
{
}

MainGUI::~MainGUI()
{

}

void MainGUI::show()
{
	m_visible = true;
}

void MainGUI::hide()
{
	m_visible = false;
}


void MainGUI::render(GLFWwindow* window)
{
	if (m_polygonMode == 0)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	if (!m_initialized)
		initGUI();

	if (!m_visible)
	{
		return;
	}

	ImGuiIO& io = ImGui::GetIO();

	mousePressed[0] = static_cast<bool>(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1));
	mousePressed[1] = static_cast<bool>(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2));

	update(window);

	bool t = true;

	static bool no_titlebar = true;
	static bool no_resize = true;
	static bool no_move = true;
	static bool no_scrollbar = false;
	static bool no_collapse = false;

	//static float bg_alpha = 1.0f;
	static float bg_alpha = 0.65f;

	ImGuiWindowFlags  window_flags = 0;
	if (no_titlebar)  window_flags |= ImGuiWindowFlags_NoTitleBar;
	if (no_resize)    window_flags |= ImGuiWindowFlags_NoResize;
	if (no_move)      window_flags |= ImGuiWindowFlags_NoMove;
	if (no_scrollbar) window_flags |= ImGuiWindowFlags_NoScrollbar;
	if (no_collapse)  window_flags |= ImGuiWindowFlags_NoCollapse;


	int width;
	int height;

	glfwGetWindowSize(window, &width, &height);
	m_height = height;

	ImGui::SetNextWindowPos(ImVec2(static_cast<float>(width - m_width), 0.0f));
	ImGui::SetNextWindowSize(ImVec2(static_cast<float>(m_width), static_cast<float>(m_height)));


	if (!ImGui::Begin(" ", &t, ImVec2(0, 0), bg_alpha, window_flags))
		ImGui::End();
	else
	{
		drawElements(window);
		ImGui::End();
	}

	glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);

	ImGui::Render();

	if (m_polygonMode == 0)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void MainGUI::setFPSVariable(int *finalFPS, double *renderTime)
{
	m_finalFPS = finalFPS;
	m_renderTime = renderTime;
}

void MainGUI::setIsolevelVariable(GLint *isolevel)
{
	m_isolevel = isolevel;
}

void MainGUI::setWireframeVariable(bool *wireframe)
{
	m_wireframe = wireframe;
}

void MainGUI::setRenderModeVariable(GLint *renderMode)
{
	m_renderMode = renderMode;
}

void MainGUI::setPolygonMode(int polygonMode)
{
	m_polygonMode = polygonMode;
}

bool MainGUI::isoValueAdapted()
{
	if (m_isoValueApplied)
	{
		m_isoValueApplied = false;
		return true;
	}
	return false;
}

bool MainGUI::loadNewDataset()
{
	if (m_loadNewDataset)
	{
		m_loadNewDataset = false;
		return true;
	}
	return false;
}

void MainGUI::setIllustrativeVisualization(IllustrativeVisualization* illuVisu) { m_IllustrativeVisu = illuVisu; }
void MainGUI::setVisualizationMode(GLuint *visualizationMode){ m_VisualizationMode = visualizationMode; }
void MainGUI::setClearColor(glm::vec3 * clearColor){ m_ClearColor = clearColor; }
void MainGUI::setLightPosition(glm::vec3 *lightPosition) { m_LightPosition = lightPosition; }

//Gooch Shading
void MainGUI::setGoochSamples(GLuint *samples){ m_GoochSamples = samples; }
void MainGUI::setGoochLineThickness(GLfloat *lineThickness){ m_GoochLineThickness = lineThickness; }
void MainGUI::setGoochSobelTreshhold(GLfloat *sobelTreshhold){ m_GoochSobelTreshhold = sobelTreshhold; }
void MainGUI::setGoochDiffuseColor(glm::vec3 *diffuseColor){ m_GoochDiffuseColor = diffuseColor; }
void MainGUI::setGoochSpecularColor(glm::vec3 *specularColor){ m_GoochSpecularColor = specularColor; }
void MainGUI::setGoochAmbientColor(glm::vec3 *ambientColor){ m_GoochAmbientColor = ambientColor; }
void MainGUI::setGoochCoolColor(glm::vec3 *coolColor){ m_GoochCoolColor = coolColor; }
void MainGUI::setGoochWarmColor(glm::vec3 *warmColor){ m_GoochWarmColor = warmColor; }
void MainGUI::setGoochDiffuseCoolInfluence(GLfloat *coolInfluence){ m_GoochDiffuseCoolInfluence = coolInfluence; }
void MainGUI::setGoochDiffuseWarmInfluence(GLfloat *warmInfluence){ m_GoochDiffuseWarmInfluence = warmInfluence; }

//Contours Shading
void MainGUI::setContoursLineThickness(GLfloat *lineThickness){ m_ContoursLineThickness = lineThickness; }
void MainGUI::setContoursLineColor(glm::vec3 *lineColor){ m_ContoursLineColor = lineColor; }
void MainGUI::setContoursSolidColor(glm::vec3 *solidColor){ m_ContoursSolidColor = solidColor; }
void MainGUI::setContoursSolidAlpha(GLfloat *solidAlpha){ m_ContoursSolidAlpha = solidAlpha; }
void MainGUI::setContoursUseSolidColor(bool *solidState) { m_ContoursUseSolidColor = solidState; }

//Hatching
void MainGUI::setHatchingTheta(float *theta) { m_HatchingTheta = theta; }
void MainGUI::setHatchingPatternSize(float *patternSize) { m_HatchingPatternSize = patternSize; }
void MainGUI::setHatchingIntensity(int *intensity) { m_HatchingIntensity = intensity; }
void MainGUI::setHatchingColor(glm::vec3 *color) { m_HatchingColor = color; }
void MainGUI::setHatchingAlpha(float *alpha) { m_HatchingAlpha = alpha; }
void MainGUI::setHatchingFillColor(glm::vec3 *fillColor){ m_HatchingFillColor = fillColor; }
void MainGUI::setHatchingFillAlpha(float *fillAlpha){ m_HatchingFillAlpha = fillAlpha; }
void MainGUI::setHatchingUseFillColor(bool *useFillColor){ m_HatchingUseFillColor = useFillColor; }

//Stippling
void MainGUI::setStipplingColor(glm::vec3 *color) { m_StipplingColor = color; }
void MainGUI::setStipplingAlpha(float *alpha) { m_StipplingAlpha = alpha; }
void MainGUI::setStipplingFillColor(glm::vec3 *fillColor){ m_StipplingFillColor = fillColor; }
void MainGUI::setStipplingFillAlpha(float *fillAlpha){ m_StipplingFillAlpha = fillAlpha; }
void MainGUI::setStipplingUseFillColor(bool *useFillColor){ m_StipplingUseFillColor = useFillColor; }
void MainGUI::setStipplingPointer(Stippling *stippling) { m_stippling = stippling; }


//Cel Shading
void MainGUI::setCelDiffuseMaterial(glm::vec3 *diffuseMaterial) { m_CelDiffuseMaterial = diffuseMaterial; }
void MainGUI::setCelAmbientMaterial(glm::vec3 *ambientMaterial) { m_CelAmbientMaterial = ambientMaterial; }
void MainGUI::setCelSpecularMaterial(glm::vec3 *specularMaterial) { m_CelSpecularMaterial = specularMaterial; }
void MainGUI::setCelShininess(float *shininess) { m_CelShininess = shininess; }

void MainGUI::drawElements(GLFWwindow* window)
{

	if (m_renderTextA.size() == 0)
	{
		m_renderTextA = "OpenGL-Version: ";
		m_renderTextB = "GLSL-Version: ";
		m_renderTextC = "GPU: ";

		m_renderTextA.append(reinterpret_cast<char const *>(glGetString(GL_VERSION)));
		m_renderTextB.append(reinterpret_cast<char const *>(glGetString(GL_SHADING_LANGUAGE_VERSION)));
		m_renderTextC.append(reinterpret_cast<char const *>(glGetString(GL_RENDERER)));

		//Cut off after last zero of version number
		if (m_renderTextA.find("0 ") != std::string::npos)
			m_renderTextA = m_renderTextA.substr(0, (m_renderTextA.find("0 ")) + 1);
		if (m_renderTextB.find("") != std::string::npos)
			m_renderTextB = m_renderTextB.substr(0, (m_renderTextB.find("0 ")) + 1);

	}
    ImGui::Spacing();
    ImGui::Spacing();
	if (ImGui::CollapsingHeader("System Information"))
	{
		
		//RENDERER INFO
		ImGui::Spacing();
		ImGui::Spacing();
		tab(); ImGui::Text(m_renderTextA.c_str());
		tab(); ImGui::Text(m_renderTextB.c_str());
		tab(); ImGui::Text(m_renderTextC.c_str());
        ImGui::Spacing();
        ImGui::Spacing();
	}
	if (ImGui::CollapsingHeader("Data"))
	{
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		ImGui::Text("Marching Cubes Isolevel");
		tab();
		ImGui::SliderInt("##Isolevel", m_isolevel, -999, 3000);
		ImGui::SameLine();
		if (ImGui::Button("Apply"))
		{
			m_isoValueApplied = true;
		}
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		if (ImGui::Button("Load new Dataset"))
		{
			m_loadNewDataset = true;
		}
		ImGui::Spacing();
		ImGui::Spacing();
	}
	if (ImGui::CollapsingHeader("Visualization"))
	{
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		ImGui::Text("Background Color");
		tab();
		ImGui::ColorEdit3("##BackgroundColor", (float*)m_ClearColor);
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		ImGui::Text("Light Position");
		tab();
		ImGui::DragFloat3("##LightPosition", (float *)m_LightPosition, 0.1f, -5.0f, 5.0f, "%.1f");
		ImGui::Spacing();
		ImGui::Spacing();
		tab();
		ImGui::Checkbox("Wireframe", m_wireframe);
		ImGui::Spacing();
		ImGui::Spacing();

		tab();
		ImGui::Text("Visualization Mode");
		const char* namesVMode[] = { "Simple Shading", "Illustrative" };
		tab();
		ImGui::Combo("##VisualizationMode", (GLint*)m_VisualizationMode, namesVMode, ((int)(sizeof(namesVMode) / sizeof(*namesVMode))));
		ImGui::Spacing();
		ImGui::Spacing();

		switch (*m_VisualizationMode)
		{
		case MyEnums::SIMPLE_SHADING: {
			tab();
			ImGui::Text("Mode");
			const char* names[] = { "Positions", "Normals", "Color" };
			tab();
			ImGui::Combo("##RenderMode", m_renderMode, names, ((int)(sizeof(names) / sizeof(*names))));
			ImGui::Spacing();
			ImGui::Spacing();

			break;
		}

		case MyEnums::ILLUSTRATIVE_VISUALIZATION: {

			tab();
			if (ImGui::Checkbox("Gooch Shading", &IllustrativeVisualization::USE_GOOCH_SHADING)) {
				if (!IllustrativeVisualization::USE_GOOCH_SHADING) m_IllustrativeVisu->getGoochShading()->resetFramebuffer();
			}
			tab();
			ImGui::Checkbox("Contours", &IllustrativeVisualization::USE_CONTOURS_SHADING);
			tab();
			ImGui::Checkbox("Hatching", &IllustrativeVisualization::USE_HATCHING);
			tab();
			ImGui::Checkbox("Cel Shading", &IllustrativeVisualization::USE_CELSHADING);
            tab();
            ImGui::Checkbox("Stippling", &IllustrativeVisualization::USE_STIPPLING);

			if (IllustrativeVisualization::USE_GOOCH_SHADING)
			{
				tab();

				if (ImGui::CollapsingHeader("Gooch Settings")) {
					tab();
					tab();
					ImGui::Text("Multi Sampling");
					tab();
					tab();
					ImGui::SliderInt("Samples##GoochSamples", (int *)m_GoochSamples, 1, 4);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
					ImGui::Text("Line Settings");
					tab();
					tab();
					ImGui::SliderFloat("Width##GoochLineWidth", m_GoochLineThickness, 1.0f, 10.0f);
					ImGui::Spacing();
					tab();
					tab();
					ImGui::SliderFloat("Details##GoochDetails", m_GoochSobelTreshhold, 0.0f, 1.0f);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
					ImGui::Text("Gooch Color");
					tab();
					tab();
					ImGui::ColorEdit3("Cool##GoochCoolColor", (float *)m_GoochCoolColor);
					ImGui::Spacing();
					tab();
					tab();
					ImGui::ColorEdit3("Warm##GoochWarmColor", (float *)m_GoochWarmColor);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
					ImGui::Text("Diffuse Color Influence");
					tab();
					tab();
					ImGui::SliderFloat("Cool##GoochCoolInfluence", m_GoochDiffuseCoolInfluence, 0.0f, 1.0f);
					ImGui::Spacing();
					tab();
					tab();
					ImGui::SliderFloat("Warm##GoochWarmInfluence", m_GoochDiffuseWarmInfluence, 0.0f, 1.0f);
					ImGui::Spacing();
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();

					if (ImGui::CollapsingHeader("Material Attributes"))
					{
						ImGui::Spacing();
						ImGui::Spacing();
						tab();
						tab();
						ImGui::ColorEdit3("Diffuse##GoochDiffuseColor", (float *)m_GoochDiffuseColor);
						ImGui::Spacing();
						tab();
						tab();
						ImGui::ColorEdit3("Specular##GoochSpecularColor", (float *)m_GoochSpecularColor);
						ImGui::Spacing();
						tab();
						tab();
						ImGui::ColorEdit3("Ambient##GoochAmbientColor", (float *)m_GoochAmbientColor);
					}
				}
			}

			if (IllustrativeVisualization::USE_CONTOURS_SHADING)
			{
				tab();
				if (ImGui::CollapsingHeader("Contour Settings")) {

					tab();
					tab();
					ImGui::Text("Line Settings");
					tab();
					tab();
					ImGui::ColorEdit3("Color##ContoursLineColor", (float *)m_ContoursLineColor);
					ImGui::Spacing();
					tab();
					tab();
					ImGui::SliderFloat("Width##ContoursLineWidth", m_ContoursLineThickness, 1.0f, 10.0f);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
					ImGui::Checkbox("Use Fill Color", m_ContoursUseSolidColor);

					if (*m_ContoursUseSolidColor)
					{
						tab();
						tab();
						ImGui::Text("Fill Settings");
						tab();
						tab();
						ImGui::ColorEdit3("Color##ContoursSolidColor", (float *)m_ContoursSolidColor);
						ImGui::Spacing();
						tab();
						tab();
						ImGui::SliderFloat("Alpha##ContoursSolidAlpha", m_ContoursSolidAlpha, 0.0f, 1.0f);
					}
				}
			}

			if (IllustrativeVisualization::USE_HATCHING)
			{
				tab();
				if (ImGui::CollapsingHeader("Hatching Settings")) {

					tab();
					tab();
					ImGui::SliderFloat("Rotation##Hatching", m_HatchingTheta, -5.0f, 5.0f);
					tab();
					tab();
					ImGui::SliderInt("Intensity##Hatching", m_HatchingIntensity, -20, 20);
					tab();
					tab();
					ImGui::SliderFloat("Size##Hatching", m_HatchingPatternSize, 1.0f, 50.0f);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
                    ImGui::Text("Line Settings");
                    tab();
                    tab();
                    ImGui::ColorEdit3("Color##HatchingColor", (float *)m_HatchingColor);
					tab();
					tab();
					ImGui::SliderFloat("Alpha##HatchingAlpha", m_HatchingAlpha, 0.0f, 1.0f);
					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
					ImGui::Checkbox("Use Fill Color##Hatching", m_HatchingUseFillColor);

					if (*m_HatchingUseFillColor) {
						tab();
						tab();
						ImGui::Text("Fill Settings");
						tab();
						tab();
						ImGui::ColorEdit3("Color##HatchingFillColor", (float *)m_HatchingFillColor);
						ImGui::Spacing();
						tab();
						tab();
						ImGui::SliderFloat("Alpha##HatchingFillAlpha", m_HatchingFillAlpha, 0.0f, 1.0f);
					}
				}
			}

			if (IllustrativeVisualization::USE_CELSHADING)
			{
				tab();
				if (ImGui::CollapsingHeader("Cel Shader Settings"))
				{
					tab();
					tab();
					ImGui::ColorEdit3("Diffuse##CelDiffuseMaterial", (float *)m_CelDiffuseMaterial);
					tab();
					tab();
					ImGui::ColorEdit3("Ambient##CelAmbientMaterial", (float *)m_CelAmbientMaterial);
					tab();
					tab();
					ImGui::ColorEdit3("Specular##CelSpecularMaterial", (float *)m_CelSpecularMaterial);
					tab();
					tab();
					ImGui::SliderFloat("Shininess", m_CelShininess, 0.0f, 1.0f);
				}
			}

            if (IllustrativeVisualization::USE_STIPPLING)
            {
                tab();
                if (ImGui::CollapsingHeader("Stippling Settings"))
                {
					tab();
					tab();
					ImGui::Text("Noise Texture Resolution");
					tab();
                    tab();
					ImGui::SliderInt("Width##Stippling", &m_StipplingResX, 10, 350);
					tab();
					tab();
					ImGui::SliderInt("Height##Stippling", &m_StipplingResY, 10, 350);
					tab();
					tab();
					ImGui::SliderInt("Depth##Stippling", &m_StipplingResZ, 10, 350);
					tab();
					tab();
					if(ImGui::Button("Apply"))
					{
						m_stippling->createNoiseTexture(m_StipplingResX,m_StipplingResY,m_StipplingResZ);
					}

					ImGui::Spacing();
					ImGui::Spacing();
					tab();
					tab();
                    ImGui::Text("Line Settings");
                    tab();
                    tab();
					ImGui::ColorEdit3("Color##HatchingColor", (float *)m_StipplingColor);
                    tab();
                    tab();
                    ImGui::SliderFloat("Alpha##HatchingAlpha", m_StipplingAlpha, 0.0f, 1.0f);
                    ImGui::Spacing();
                    ImGui::Spacing();
                    tab();
                    tab();
                    ImGui::Checkbox("Use Fill Color##Hatching", m_StipplingUseFillColor);

                    if (*m_StipplingUseFillColor) {
                        tab();
                        tab();
                        ImGui::Text("Fill Settings");
                        tab();
                        tab();
                        ImGui::ColorEdit3("Color##HatchingFillColor", (float *)m_StipplingFillColor);
                        ImGui::Spacing();
                        tab();
                        tab();
                        ImGui::SliderFloat("Alpha##HatchingFillAlpha", m_StipplingFillAlpha, 0.0f, 1.0f);
                    }


                }
            }

			break;
		}
		default: {
			break;
		}
		}
	}
}

std::string MainGUI::doubleDigitToString(double& number)
{
	std::string returnVal = std::to_string(number);

	if (returnVal.find(".") == 1)
		return std::string("  ").append(returnVal);

	return returnVal;
}