#include "GoochShading.h"

using namespace glm;

GoochShading::GoochShading(){
	uiSamples = 2;
	uiSamplesTemp = uiSamples;

	uiMeshBuffer = 0;
	uiNormalBuffer = 0;
	uiVertexCount = 0;
	mModelMatrix = mat4(1.0f);

	fLineThickness = 3.0f;
	fSobelTreshhold = 0.0f;

	vDiffuseColor = vec3(1.0f, 0.0f, 0.0f);
	vSpecularColor = vec3(0.0f, 0.0f, 0.0f);	
	vAmbientColor = vec3(0.1f, 0.1f, 0.1f);

	vGoochCoolColor = vec3(0, 0.0f, 1.0f);
	vGoochWarmColor = vec3(1.0f, 1.0f, 0.0f);

	fDiffuseCoolInfluence = 1.0f;
	fDiffuseWarmInfluence = 1.0f;

	vLightPosition = new vec3(2.0f, 2.0f, 2.0f);

	uiWindowWidth = 0;
	uiWindowHeight = 0;

	bFrameBufferUpdatedFirstPass = GL_FALSE;

	//init Screen Quad Buffer
	GLfloat fScreenQuad[] = {
		-1.0f, -1.0f, 0.0f, -1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f
	};

	GLfloat fScreenQuadUV[] = {
		0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f
	};

	GLuint uiScreenQuadSize = sizeof(fScreenQuad);
	MyOpenGL::createVertexBuffer(1, &uiScreenQuadBuffer, uiScreenQuadSize, fScreenQuad);

	GLuint uiScreenQuadUVSize = sizeof(fScreenQuadUV);
	MyOpenGL::createVertexBuffer(1, &uiScreenQuadUVBuffer, uiScreenQuadUVSize, fScreenQuadUV);
}

GoochShading::~GoochShading(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	resetFramebuffer();
}

void GoochShading::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
	uiMeshBuffer = meshBuffer;
	uiNormalBuffer = normalBuffer;
	uiVertexCount = vertexCount;
	mModelMatrix = modelMatrix;
	renderProgram = program;
}

void GoochShading::render(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	if (uiWindowWidth != windowWidth || uiWindowHeight != windowHeight || uiSamplesTemp != uiSamples){
		uiWindowWidth = windowWidth;
		uiWindowHeight = windowHeight;
		uiSamplesTemp = uiSamples;

		resetFramebuffer();
	}

	renderFirstPass(camera, windowWidth, windowHeight);
	renderSecondPass(camera, windowWidth, windowHeight);
	renderFinalPass(camera, windowWidth, windowHeight);
}

void GoochShading::setLightPosition(glm::vec3 *lightPosition){
	vLightPosition = lightPosition;
}

void GoochShading::resetFramebuffer(){
	bFrameBufferUpdatedFirstPass = GL_FALSE;
	bFrameBufferUpdatedSecondPass = GL_FALSE;

	glDeleteFramebuffers(1, &uiFrameBufferFirstPass);
	glDeleteRenderbuffers(1, &uiDepthBufferFirstPass);

	glDeleteFramebuffers(1, &uiFrameBufferSecondPass);
	glDeleteRenderbuffers(1, &uiDepthBufferSecondPass);

	glDeleteTextures(1, &uiDepthTexture);
	glDeleteTextures(1, &uiNormalTexture);
	glDeleteTextures(1, &uiColorTexture);
	glDeleteTextures(1, &uiSecondPassTexture);
}

//Render First Pass
void GoochShading::renderFirstPass(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = renderProgram;

	//Draw to Texture
	glEnable(GL_DEPTH_TEST);

	startRenderFirstPass();
	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Set Uniform
	//vLightPosition = camera->getCameraPos();
	glUniform1i(glGetUniformLocation(program, "useGoochFirst"), true);

	glUniform4f(glGetUniformLocation(program, "lightPosition"), vLightPosition->x, vLightPosition->y, vLightPosition->z, 1.0f);

	glUniform3f(glGetUniformLocation(program, "matAttr.specularColor"), vSpecularColor.x, vSpecularColor.y, vSpecularColor.z);
	glUniform3f(glGetUniformLocation(program, "matAttr.diffuseColor"), vDiffuseColor.x, vDiffuseColor.y, vDiffuseColor.z);
	glUniform3f(glGetUniformLocation(program, "matAttr.ambientColor"), vAmbientColor.x, vAmbientColor.y, vAmbientColor.z);

	glUniform3f(glGetUniformLocation(program, "matAttr.goochCool"), vGoochCoolColor.x, vGoochCoolColor.y, vGoochCoolColor.z);	
	glUniform3f(glGetUniformLocation(program, "matAttr.goochWarm"), vGoochWarmColor.x, vGoochWarmColor.y, vGoochWarmColor.z);

	glUniform1f(glGetUniformLocation(program, "matAttr.diffuseCoolInfluence"), fDiffuseCoolInfluence);
	glUniform1f(glGetUniformLocation(program, "matAttr.diffuseWarmInfluence"), fDiffuseWarmInfluence);

	//Set Transform Matrices
	MyOpenGL::setMatrices(program, mModelMatrix, camera);

	glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);
	endRenderPass();

	glDisable(GL_DEPTH_TEST);

	glUniform1i(glGetUniformLocation(program, "useGoochFirst"), false);
}

GLint GoochShading::updateFrameBufferFirstPass(){
	GLint returnValue = GL_TRUE;
	glDeleteFramebuffers(1, &uiFrameBufferFirstPass);
	glDeleteRenderbuffers(1, &uiDepthBufferFirstPass);

	//Frame Buffer
	glGenFramebuffers(1, &uiFrameBufferFirstPass);
	glBindFramebuffer(GL_FRAMEBUFFER, uiFrameBufferFirstPass);

	//Depth Texture and Attachmend
	glGenRenderbuffers(1, &uiDepthBufferFirstPass);
	glBindRenderbuffer(GL_RENDERBUFFER, uiDepthBufferFirstPass);
	MyOpenGL::create2DTexture(GL_TEXTURE0, &uiDepthTexture, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, GL_DEPTH_COMPONENT24,
		uiWindowWidth * uiSamples, uiWindowHeight * uiSamples, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, uiDepthTexture, 0);

	GLuint uiTextureNumber = 2;

	//Color Attachmend and Draw Buffers
	MyOpenGL::create2DTexture(GL_TEXTURE0, &uiColorTexture, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, GL_RGBA,
		uiWindowWidth * uiSamples, uiWindowHeight * uiSamples, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, uiColorTexture, 0);

	MyOpenGL::create2DTexture(GL_TEXTURE0, &uiNormalTexture, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, GL_RGBA,
		uiWindowWidth * uiSamples, uiWindowHeight * uiSamples, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, uiNormalTexture, 0);

	//Draw Buffers
	GLenum* drawBuffers = new GLenum[uiTextureNumber];
	for (int i = 0; i < uiTextureNumber; i++){
		drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
	}
	glDrawBuffers(uiTextureNumber, drawBuffers);
	delete[] drawBuffers;

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		returnValue = GL_FALSE;

	bFrameBufferUpdatedFirstPass = GL_TRUE;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return returnValue;
}

void GoochShading::startRenderFirstPass(){
	if (!bFrameBufferUpdatedFirstPass) updateFrameBufferFirstPass();

	glBindFramebuffer(GL_FRAMEBUFFER, uiFrameBufferFirstPass);
	glViewport(0, 0, uiWindowWidth * uiSamples, uiWindowHeight * uiSamples);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//Render Second Pass
void GoochShading::renderSecondPass(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = renderProgram;

	//Draw Screen Filling Quad
	startRenderSecondPass();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiScreenQuadBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiScreenQuadUVBuffer);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//Bind Textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, uiDepthTexture);
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, uiNormalTexture);
	glActiveTexture(GL_TEXTURE0 + 2);
	glBindTexture(GL_TEXTURE_2D, uiColorTexture);

	//Set Uniforms
	glUniform1i(glGetUniformLocation(program, "useGoochSecond"), true);

	glUniform1i(glGetUniformLocation(program, "depthTex"), 0);
	glUniform1i(glGetUniformLocation(program, "normalTex"), 1);
	glUniform1i(glGetUniformLocation(program, "colorTex"), 2);

    glUniform1i(glGetUniformLocation(program, "screenWidth"), static_cast<int>(uiWindowWidth * uiSamples));
    glUniform1i(glGetUniformLocation(program, "screenHeight"), static_cast<int>(uiWindowHeight * uiSamples));

	GLfloat lineWidth = fLineThickness * (uiSamples / 4.0f);
	glUniform1f(glGetUniformLocation(program, "lineThickness"), lineWidth);
	glUniform1f(glGetUniformLocation(program, "normalTreshhold"), fSobelTreshhold);

	glDrawArrays(GL_QUADS, 0, 4);

	endRenderPass();

	glUniform1i(glGetUniformLocation(program, "useGoochSecond"), false);
}

GLint GoochShading::updateFrameBufferSecondPass(){
	GLint returnValue = GL_TRUE;
	glDeleteFramebuffers(1, &uiFrameBufferSecondPass);
	glDeleteRenderbuffers(1, &uiDepthBufferSecondPass);

	//Frame Buffer
	glGenFramebuffers(1, &uiFrameBufferSecondPass);
	glBindFramebuffer(GL_FRAMEBUFFER, uiFrameBufferSecondPass);

	//Depth Texture and Attachmend
	glGenRenderbuffers(1, &uiDepthBufferSecondPass);
	glBindRenderbuffer(GL_RENDERBUFFER, uiDepthBufferSecondPass);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, uiWindowWidth * uiSamples, uiWindowHeight * uiSamples);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, uiDepthBufferSecondPass);

	GLuint uiTextureNumber = 1;

	//Color Attachmend and Draw Buffers
	MyOpenGL::create2DTexture(GL_TEXTURE0, &uiSecondPassTexture, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, GL_RGBA,
		uiWindowWidth * uiSamples, uiWindowHeight * uiSamples, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, uiSecondPassTexture, 0);

	//Draw Buffers
	GLenum* drawBuffers = new GLenum[uiTextureNumber];
	for (int i = 0; i < uiTextureNumber; i++){
		drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
	}
	glDrawBuffers(uiTextureNumber, drawBuffers);
	delete[] drawBuffers;

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		returnValue = GL_FALSE;

	bFrameBufferUpdatedSecondPass = GL_TRUE;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return returnValue;
}

void GoochShading::startRenderSecondPass(){
	if (!bFrameBufferUpdatedSecondPass) updateFrameBufferSecondPass();

	glBindFramebuffer(GL_FRAMEBUFFER, uiFrameBufferSecondPass);
	glViewport(0, 0, uiWindowWidth * uiSamples, uiWindowHeight * uiSamples);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

//Render Final Pass
void GoochShading::renderFinalPass(Camera* camera, GLuint windowWidth, GLuint windowHeight){
	GLuint program = renderProgram;

	//Draw Screen Filling Quad
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glUseProgram(program);

	//Bind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, uiScreenQuadBuffer);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiScreenQuadUVBuffer);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//Bind Textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, uiSecondPassTexture);

	//Set Uniforms
	glUniform1i(glGetUniformLocation(program, "useGoochFinal"), true);
	glUniform1i(glGetUniformLocation(program, "secondPassTex"), 0);

    glUniform1i(glGetUniformLocation(program, "screenWidth"), static_cast<int>(uiWindowWidth));
    glUniform1i(glGetUniformLocation(program, "screenHeight"), static_cast<int>(uiWindowHeight));
    glUniform1i(glGetUniformLocation(program, "samples"), static_cast<int>(uiSamples));

	glDrawArrays(GL_QUADS, 0, 4);

	glUniform1i(glGetUniformLocation(program, "useGoochFinal"), false);
}

void GoochShading::endRenderPass(){
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, uiWindowWidth, uiWindowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}