#ifndef GOOCHSHADING_H
#define GOOCHSHADING_H

#include "MyResources.h"
#include "MyOpenGL.h"
#include "Camera.h"

class GoochShading
{
public:
	GoochShading();
	~GoochShading();

	void initData(GLuint meshBuffer, GLuint normalBuffer, GLuint vertexCount, glm::mat4 modelMatrix, GLuint program);

	void render(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	void setLightPosition(glm::vec3 *lightPosition);

	void resetFramebuffer();

	//Material Attributes
	GLuint uiSamples;

	GLfloat fLineThickness;
	GLfloat fSobelTreshhold;

	glm::vec3 vDiffuseColor;
	glm::vec3 vSpecularColor;
	glm::vec3 vAmbientColor;
	glm::vec3 vGoochCoolColor;
	glm::vec3 vGoochWarmColor;

	GLfloat fDiffuseCoolInfluence;
	GLfloat fDiffuseWarmInfluence;

private:
	/*************************************************************************
	* private methods
	*************************************************************************/
	//Render First Pass
	void renderFirstPass(Camera* camera, GLuint windowWidth, GLuint windowHeight);
	GLint updateFrameBufferFirstPass();
	void startRenderFirstPass();	

	//Render Second Pass
	void renderSecondPass(Camera* camera, GLuint windowWidth, GLuint windowHeight);
	GLint updateFrameBufferSecondPass();
	void startRenderSecondPass();

	//Render Final Pass
	void renderFinalPass(Camera* camera, GLuint windowWidth, GLuint windowHeight);

	//General
	void endRenderPass();

	/*************************************************************************
	* private variables
	*************************************************************************/
	//General Settings
	glm::vec3 *vLightPosition;
	GLuint uiSamplesTemp;
	GLuint uiWindowWidth;
	GLuint uiWindowHeight;

	//Mesh Data
	GLuint uiMeshBuffer;
	GLuint uiNormalBuffer;
	GLuint uiVertexCount;
	glm::mat4 mModelMatrix;

	//Shader Programs
	GLuint renderProgram;

	//Framebuffer First Pass
	GLuint uiFrameBufferFirstPass;
	GLuint uiDepthBufferFirstPass;
	GLboolean bFrameBufferUpdatedFirstPass;

	//Framebuffer Textures First Pass
	GLuint uiDepthTexture;
	GLuint uiNormalTexture;
	GLuint uiColorTexture;

	//Framebuffer Second Pass
	GLuint uiFrameBufferSecondPass;
	GLuint uiDepthBufferSecondPass;
	GLboolean bFrameBufferUpdatedSecondPass;

	//Framebuffer Textures Second Pass
	GLuint uiSecondPassTexture;

	//Screen Quad Buffer
	GLuint uiScreenQuadBuffer;
	GLuint uiScreenQuadUVBuffer;

};

#endif // GOOCHSHADING_H