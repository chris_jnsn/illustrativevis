#include "Stippling.h"

using namespace glm;

Stippling::Stippling(){
    uiMeshBuffer = 0;
    uiNormalBuffer = 0;
    uiUVBuffer = 0;
    uiVertexCount = 0;
    mModelMatrix = mat4(1.0f);
    m_noiseTexture = 0;
    m_stipplingColor = glm::fvec3(0.0f, 0.0f, 0.0f);
    m_stipplingAlpha = 1.0f;
    m_fillColor = glm::fvec3(1.0f, 1.0f, 1.0f);
    m_fillAlpha = 1.0f;
    m_useFillColor = false;

    uiWindowWidth = 0;
    uiWindowHeight = 0;
}

Stippling::~Stippling(){

}

void Stippling::createNoiseTexture(unsigned int w, unsigned int h, unsigned int d)
{
    glDeleteTextures(1,&m_noiseTexture);

    std::vector<GLubyte> data = std::vector<GLubyte>();
    unsigned int const textureSize = w * h * d;
    
    //create noise texture (all values between 0 and 1)
    for (int i = 0; i < textureSize; i++)
    {
        static_cast<float>(rand()) / static_cast<float>(RAND_MAX) < 0.5 ? data.push_back(0) : data.push_back(255);
    }

    glEnable(GL_TEXTURE_3D);
    GLuint noiseTexture;
    glGenTextures(1, &noiseTexture);
    glBindTexture(GL_TEXTURE_3D, noiseTexture);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_LUMINANCE16, w, h, d, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, &data[0]);

    m_noiseTexture = noiseTexture;
}

void Stippling::initData(GLuint meshBuffer, GLuint normalBuffer, GLuint uvBuffer, GLuint vertexCount, mat4 modelMatrix, GLuint program){
    uiMeshBuffer = meshBuffer;
    uiNormalBuffer = normalBuffer;
    uiUVBuffer = uvBuffer;
    uiVertexCount = vertexCount;
    mModelMatrix = modelMatrix;
	renderProgram = program;

    createNoiseTexture(300,300,300);
}

void Stippling::setLightPosition(glm::vec3 *lightPosition){
    vLightPosition = lightPosition;
}

void Stippling::render(Camera* camera, GLuint windowWidth, GLuint windowHeight)
{

    if (uiWindowWidth != windowWidth || uiWindowHeight != windowHeight)
    {
        uiWindowWidth = windowWidth;
        uiWindowHeight = windowHeight;
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GLuint program = renderProgram;

    //Draw to Texture
    glEnable(GL_DEPTH_TEST);
	
    glUseProgram(program);

    //Bind Buffer
    glBindBuffer(GL_ARRAY_BUFFER, uiMeshBuffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, uiNormalBuffer);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, uiUVBuffer);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //HATCHING UNIFORMS==========================================================================================
  
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, m_noiseTexture);

    glUniform1i(glGetUniformLocation(program, "whiteNoiseTexture"), 0);

    glUniform4f(glGetUniformLocation(program, "lightPosition"), vLightPosition->x, vLightPosition->y, vLightPosition->z, 1.0f);

    glm::fvec4 stipplingC = glm::fvec4(m_stipplingColor.x, m_stipplingColor.y, m_stipplingColor.z, m_stipplingAlpha);
    glUniform4fv(glGetUniformLocation(program, "stipplingColor"), 1, glm::value_ptr(stipplingC));


    glm::fvec4 fillC;
    if (m_useFillColor)
        fillC = glm::fvec4(m_fillColor.x, m_fillColor.y, m_fillColor.z, m_fillAlpha);
    else
        fillC = glm::fvec4(m_fillColor.x, m_fillColor.y, m_fillColor.z, 0.0);

    glUniform4fv(glGetUniformLocation(program, "fillColor"), 1, glm::value_ptr(fillC));

    //===========================================================================================================

    //Set Transform Matrices
    MyOpenGL::setMatrices(program, mModelMatrix, camera);

    glDrawArrays(GL_TRIANGLES, 0, uiVertexCount);
 
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}
