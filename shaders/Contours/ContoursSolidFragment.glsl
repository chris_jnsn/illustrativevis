#version 330

out vec4 fragmentColor;

uniform vec4 solidColor;

void main(){
	fragmentColor = solidColor;
}
