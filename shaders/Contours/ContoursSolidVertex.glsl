#version 330

layout (location = 0) in vec4 positionAttribute;

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

mat4 modelViewProjMatrix;

void main(){
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
    gl_Position = modelViewProjMatrix * positionAttribute;
}