#version 420
#extension GL_EXT_gpu_shader4 : enable

layout (triangles) in; 
layout (line_strip, max_vertices = 2) out;

in VertexData {
	float normalEyeDot;
	vec3 normal;
	vec3 position;
} vertex[];

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

uniform mat4 uniformNormal;

mat4 modelViewProjMatrix;

vec3 vertexInterp(vec3 v0, vec3 v1, float v0Val, float v1Val){
	v0Val = (v0Val + 1.0f) / 2.0f;
	v1Val = (v1Val + 1.0f) / 2.0f;
	
	float temp;
	if((v1Val - v0Val) == 0) temp = 1.0f;
	else temp = (1.0f - v1Val) / (v0Val - v1Val);
	
	if(temp < 0.0f) temp = 0.0f;
	else if(temp > 1.0f) temp = 1.0f;
	
	return mix(v0, v1, temp);
}

void drawVertex(int i, int j){
	vec3 pos = vertexInterp(vertex[i].position, vertex[j].position, vertex[i].normalEyeDot, vertex[j].normalEyeDot);
	gl_Position = modelViewProjMatrix * vec4(pos, 1);
	EmitVertex();
}

void drawVertices(){
	bool value0 = false;
	bool value1 = false;
	bool value2 = false;
	
	if(vertex[0].normalEyeDot <= 0.0f) value0 = true;
	if(vertex[1].normalEyeDot <= 0.0f) value1 = true;
	if(vertex[2].normalEyeDot <= 0.0f) value2 = true;

	if(value0 && !value1 && !value2){
		drawVertex(0, 1);
		drawVertex(0, 2);
	}
	else if(!value0 && value1 && !value2){
		drawVertex(1, 0);
		drawVertex(1, 2);
	}
	else if(!value0 && !value1 && value2){
		drawVertex(2, 0);
		drawVertex(2, 1);
	}	
	else if(value0 && value1 && !value2){
		drawVertex(2, 0);
		drawVertex(2, 1);
	}	
	else if(!value0 && value1 && value2){
		drawVertex(0, 1);
		drawVertex(0, 2);
	}	
	else if(value0 && !value1 && value2){
		drawVertex(1, 0);
		drawVertex(1, 2);
	}	

	EndPrimitive();		
}

void main(){
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	drawVertices();
}