#version 330

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec4 normalAttribute;

out VertexData {
	float normalEyeDot;
	vec3 normal;
	vec3 position;
} vertex;

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

uniform mat4 uniformNormal;

mat4 modelViewProjMatrix;

void main(){
	vec3 normal = normalize((uniformNormal * normalAttribute).xyz);
	vec3 view = normalize((uniformView * uniformModel * positionAttribute).xyz);
	vertex.normalEyeDot = dot(normal, view);
	
	float test = length(cross(normal, view));
	if(test < 0) vertex.normalEyeDot *= -1.0f;
	
	vertex.normal = normalize(normalAttribute.xyz);	
	vertex.position = positionAttribute.xyz;
	
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
    gl_Position = modelViewProjMatrix * positionAttribute;
}