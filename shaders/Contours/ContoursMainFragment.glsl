#version 330

out vec4 fragmentColor;

uniform vec3 lineColor;

void main(){
	fragmentColor = vec4(lineColor, 1.0f);
}
