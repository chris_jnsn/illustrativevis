#version 330

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec4 normalAttribute;

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;
uniform mat4 uniformNormal;

mat4 modelViewProjMatrix;

uniform vec3 DiffuseMaterial;

out vec3 EyespaceNormal;
out vec3 Diffuse;

void main()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	EyespaceNormal = mat3(uniformNormal) * vec3(normalAttribute);
	gl_Position = modelViewProjMatrix * positionAttribute;
	Diffuse = DiffuseMaterial;
}