#version 330

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec2 texCoordsAttribute;

out vec2 passTexCoords;

void main(){
	passTexCoords = texCoordsAttribute;
	gl_Position = positionAttribute;
}