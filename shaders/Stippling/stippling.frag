#version 330

in vec3 passPosition;
in vec3 passNormal;
in vec3 passUV;
in vec3 passLightPosition;

uniform sampler3D whiteNoiseTexture;

uniform vec4 fillColor;
uniform vec4 stipplingColor;

out vec4 fragmentColor;

//Light
vec3 lightDiffuse = vec3(1,1,1);
vec3 lightSpecular = vec3(1,1,1);
vec3 lightAmbient = vec3(0.5f, 0.5f, 0.5f);

vec4 stippling()
{
	//implemented following paper 'Efficent Stipple Rendering'
	//https://wwwcg.in.tum.de/fileadmin/user_upload/Lehrstuehle/Lehrstuhl_XV/Research/Publications/2007/cgv07_stipple.pdf

	//calculate intensity of current fragment (phong)
	vec3 lightVector = normalize(passLightPosition - passPosition);
	vec3 eye = normalize(-passPosition);
    vec3 reflection = normalize(reflect(-lightVector, passNormal));

	float diffuse = max(dot(passNormal, lightVector), 0);
	float specular = pow(max(dot(reflection, eye), 0), 250);
	float ambient = 0.5;
		
	vec4 diffuseColor = vec4(diffuse * lightDiffuse, 1);
	vec4 specularColor = vec4(specular * lightSpecular, 1);
	vec4 ambientColor = vec4(ambient  * lightAmbient, 1);
	
	vec4 finalColor = diffuseColor + specularColor + ambientColor;

	//calculate luminosity of fragment
	float luminosity = 0.21 * finalColor.r + 0.72 * finalColor.g + 0.07 * finalColor.b;

	//get Intensity from Noise Texture
	float intensity = texture(whiteNoiseTexture,passUV).r;

	if(intensity > luminosity)
		return stipplingColor;
	return fillColor;
}

void main()
{
		fragmentColor = stippling();
}
