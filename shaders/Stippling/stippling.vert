#version 330

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec4 normalAttribute;
layout (location = 2) in vec3 uvAttribute;

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

uniform mat4 uniformNormal;

uniform vec4 lightPosition;

out vec3 passPosition;
out vec3 passNormal;
out vec3 passUV;
out vec3 passLightPosition;

void main(){
	
	mat4 modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	
	passNormal = normalize((uniformNormal * normalAttribute).xyz);
	
	passPosition = (uniformView * uniformModel * positionAttribute).xyz;
    
	passUV = uvAttribute;

	passLightPosition = (uniformView * lightPosition).xyz;
	
	gl_Position = modelViewProjMatrix * positionAttribute;
}
