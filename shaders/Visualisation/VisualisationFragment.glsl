#version 330

//Renderingmethod variable

uniform bool useSimple;
uniform bool useGoochFirst;
uniform bool useGoochSecond;
uniform bool useGoochFinal;
uniform bool useContoursMain;
uniform bool useContoursSolid;
uniform bool useHatching;
uniform bool useCel;
uniform bool useMC;

//General in variables

in vec3 passPosition;
in vec3 passNormal;
in vec3 passLightPosition;

//Simple Shading variables
uniform int uniformMode;

//Gooch Shading variables
in vec2 passTexCoords;

uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D colorTex;

uniform int screenWidth;
uniform int screenHeight;

uniform float normalTreshhold;
uniform float lineThickness;

uniform sampler2D secondPassTex;

uniform int samples;

struct MaterialAttributes
{
	vec3 diffuseColor;
	vec3 specularColor;
	vec3 ambientColor;
	vec3 goochCool;
	vec3 goochWarm;
	float diffuseCoolInfluence;
	float diffuseWarmInfluence;
};

uniform MaterialAttributes matAttr;

//Hatching variables

uniform mat2 thetaRotation;
uniform int patternSize;
uniform float minIntensity;
uniform vec4 fillColor;
uniform vec4 hatchingColor;

//Contour shading variables

uniform vec3 lineColor;
uniform vec4 solidColor;

//Cel shading variables

in vec3 EyespaceNormal;
in vec3 Diffuse;

uniform vec3 LightPosition;

uniform vec3 AmbientMaterial;
uniform vec3 SpecularMaterial;
uniform float Shininess;

//Outputs
layout(location = 0) out vec4 fragmentColor;
layout(location = 1) out vec4 normalColor;

//Simple shading method

void simpleShading()
{
	//Light
	vec3 lightDiffuse = vec3(1,1,1);
	vec3 lightSpecular = vec3(1,1,1);
	vec3 lightAmbient = vec3(0.5f, 0.5f, 0.5f);

	if(uniformMode == 0) fragmentColor = vec4(passPosition,1);
	else if(uniformMode == 1) fragmentColor = vec4(passNormal,1);
	else {
		//compute the light vector as the normalized vector between 
		//the vertex position and the light position:
		vec3 lightVector = normalize(passLightPosition - passPosition);

		//compute the eye vector as the normalized negative vertex 
		//position in camera coordinates:
		vec3 eye = normalize(-passPosition);
    
		//compute the normalized reflection vector using 
		//GLSL's built-in reflect() function:
		vec3 reflection = normalize(reflect(-lightVector, passNormal));

		//comment in to use the phong lighting model:
		float diffuse = max(dot(passNormal, lightVector), 0);
		float specular = pow(max(dot(reflection, eye), 0), 250);
		float ambient = 0.5;
		
		//comment in to use the phong lighting model:
		vec4 diffuseColor = vec4(diffuse * lightDiffuse, 1);
		vec4 specularColor = vec4(specular * lightSpecular, 1);
		vec4 ambientColor = vec4(ambient  * lightAmbient, 1);
		
		fragmentColor = diffuseColor + specularColor + ambientColor;		
	}
}

//Hatching methods

vec4 createHatching()
{
	//implemented following paper 'Illustrative Display of Hidden Iso-Surface Structures'
	//http://www.gris.uni-tuebingen.de/people/staff/fischer/janfischer.com/publications/Fischer05_IllusTrans.pdf

	//calculate intensity I_p of current fragment
	vec3 lightVector = normalize(passLightPosition - passPosition);
	float I_p = max(dot(passNormal,lightVector),0);

	//rotate fragment coords around user defined angle theta
	vec2 thetaFragCoords = thetaRotation * gl_FragCoord.xy;

	//calculate dither coords from fragment coords
	vec2 ditherCoords;
	ditherCoords.s = mod(thetaFragCoords.x,patternSize) / patternSize;
	ditherCoords.t = mod(thetaFragCoords.y,patternSize) / patternSize;

	//calculate halftoning threshold tau
	float thresholdTau;

	if(ditherCoords.s < minIntensity)
	{
		thresholdTau = minIntensity * ditherCoords.t;
	}
	else
	{
		thresholdTau = (1 - minIntensity) * ditherCoords.s + minIntensity;
	}

	//calculate intensity I_hatching for current fragment
	if(I_p < thresholdTau)
	{
		return hatchingColor;
	}

	else
	{
		return fillColor;
	}


}

void hatching()
{
	fragmentColor = createHatching();
}

//Gooch first pass method

void goochFirstPass()
{
	normalColor = vec4(passNormal,1);
	
	//compute the light vector as the normalized vector between 
	//the vertex position and the light position:
	vec3 lightVector = normalize(passLightPosition - passPosition);

	//compute the eye vector as the normalized negative vertex 
	//position in camera coordinates:
	vec3 eye = normalize(-passPosition);
    
	//compute the normalized reflection vector using 
	//GLSL's built-in reflect() function:
	vec3 reflection = normalize(reflect(-lightVector, passNormal));

	//comment in to use the phong lighting model:
	float specular = pow(max(dot(reflection, eye), 0), 64);
	float ambient = 0.5;
		
	//comment in to use the phong lighting model:
	vec4 specColor = vec4(specular * matAttr.specularColor, 1);
	vec4 amColor = vec4(ambient  * matAttr.ambientColor, 1);
	
	float blendValue = 0.5f * dot(passNormal, lightVector) + 0.5f;
	vec3 kCoolColor = (matAttr.goochCool + matAttr.diffuseColor * (matAttr.diffuseCoolInfluence)) / (1.0f + matAttr.diffuseCoolInfluence);
	vec3 kWarmColor = (matAttr.goochWarm + matAttr.diffuseColor * (matAttr.diffuseWarmInfluence)) / (1.0f + matAttr.diffuseWarmInfluence);
	
	vec4 goochColor = vec4(mix(kCoolColor, kWarmColor, blendValue), 1);
		
	fragmentColor = goochColor + specColor + amColor;
}

//Gooch second pass methods

vec4 getDepthValue(int x, int y){
	vec2 texPos = vec2(passTexCoords.x + (x * (lineThickness / float(screenWidth))), passTexCoords.y + (y * (lineThickness / float(screenHeight))));
	
	float depth = texture(depthTex, texPos).r;
	return vec4(vec3(depth), 1);
}

vec4 getLinearizeDepthValue(int x, int y){
	float z = 2.0f * getDepthValue(x,y).r - 1.0f;
	float near = 0.1f;
	float far = 100.0f;
    float c = (2.0 * near * far) / (far + near - z * (far - near));
	
	return vec4(vec3(c), 1);
}

vec4 getNormalValue(int x, int y){
	vec4 color;
	vec2 texPos = vec2(passTexCoords.x + (x * (lineThickness / float(screenWidth))), passTexCoords.y + (y * (lineThickness / float(screenHeight))));
	
	color = texture(normalTex, texPos); 
	
	return color;
}

float getNormalGrayValue(int x, int y){
	vec4 color = getNormalValue(x,y);
	float grayColor = float(color.r) * 0.3f + float(color.g) * 0.6f + float(color.b) * 0.1f;
	return grayColor;
}

vec4 getColorValue(int x, int y){
	vec4 color;
	vec2 texPos = vec2(passTexCoords.x + (x * (1.0f  / float(screenWidth))), passTexCoords.y + (y * (1.0f  / float(screenHeight))));
	
	color = texture(colorTex, texPos); 
	
	return color;
}

vec4 getColorValueMedian(int x, int y){
	vec4 values[9];
	int k = 0;
	
	for (int j = -1; j <= 1; j++) {
		for (int i = -1; i <= 1; i++) {
			values[k] = getColorValue(j,i);
			values[k].w = float(values[k].r) * 0.3f + float(values[k].g) * 0.6f + float(values[k].b) * 0.1f;
			k++;
		}
	}
		
	for (int j = 0; j < 5; j++){
		int min = j;
		for (int l = j + 1; l < 9; l++)
			if (values[l].w < values[min].w) min = l;

		vec4 temp = values[j];
		values[j] = values[min];
		values[min] = temp;
	}
	
	return values[4];
}
		 
vec4 sobelNormals(float treshhold){	
	if(treshhold != 0){
		float gradientX = 0.0f;
		float gradientY = 0.0f;
		
		//Gradient X
		gradientX += getNormalGrayValue(-1, -1);
		gradientX += 2 * getNormalGrayValue(-1, 0);
		gradientX += getNormalGrayValue(-1, 1);
		gradientX -= getNormalGrayValue(1, -1);
		gradientX -= 2 * getNormalGrayValue(1, 0);
		gradientX -= getNormalGrayValue(1, 1);
		
		//Gradient Y
		gradientY += getNormalGrayValue(-1, 1);
		gradientY += 2 * getNormalGrayValue(0, 1);
		gradientY += getNormalGrayValue(1, 1);
		gradientY -= getNormalGrayValue(-1, -1);
		gradientY -= 2 * getNormalGrayValue(0, -1);
		gradientY -= getNormalGrayValue(1, -1);
		
		float mag = sqrt((gradientX * gradientX) + (gradientY * gradientY));
		if(mag >= 1.0f) mag = 1.0f;
		else if(mag < 0.0f) mag = 0.0f;
		
		mag *= treshhold;
		mag = 1.0f - mag;
	
		// if(mag < treshhold) mag = 0.0f;
		// else mag = 1.0f;
	
		return vec4(mag, mag, mag, 1);
	}
	else return vec4(1,1,1,1);
}

vec4 sobelDepth(){	
	float gradientX = 0.0f;
	float gradientY = 0.0f;
		
	//Gradient X
	gradientX += getLinearizeDepthValue(-1, -1).r;
	gradientX += 2 * getLinearizeDepthValue(-1, 0).r;
	gradientX += getLinearizeDepthValue(-1, 1).r;
	gradientX -= getLinearizeDepthValue(1, -1).r;
	gradientX -= 2 * getLinearizeDepthValue(1, 0).r;
	gradientX -= getLinearizeDepthValue(1, 1).r;
		
	//Gradient Y
	gradientY += getLinearizeDepthValue(-1, 1).r;
	gradientY += 2 * getLinearizeDepthValue(0, 1).r;
	gradientY += getLinearizeDepthValue(1, 1).r;
	gradientY -= getLinearizeDepthValue(-1, -1).r;
	gradientY -= 2 * getLinearizeDepthValue(0, -1).r;
	gradientY -= getLinearizeDepthValue(1, -1).r;
		
	float mag = sqrt((gradientX * gradientX) + (gradientY * gradientY));
	if(mag >= 1.0f) mag = 1.0f;
	else if(mag < 0.0f) mag = 0.0f;
		
	mag = 1.0f - mag;
	
	return vec4(mag, mag, mag, 1);
}

vec4 goochShading(){
	vec4 result = getColorValue(0,0) * sobelDepth() * sobelNormals(normalTreshhold);
	return result;
}

void goochSecondPass()
{
	fragmentColor = goochShading();
}

//Gooch final pass methods

vec3 getMultisampleColor(sampler2D inputTex, vec2 texCoords){
	float dx = 1.0f / float(screenWidth * samples);
	float dy = 1.0f / float(screenHeight * samples);
		
	vec3 Color = vec3(0,0,0);
		
	for(int y = 0; y < samples; y++){
		for(int x = 0; x < samples; x++){
			Color += texture(inputTex, texCoords.st + vec2(dx * x, dy * y)).rgb;
		}
	}
		
	Color /= (samples * samples);
	return Color;
}

void goochFinalPass()
{
	vec4 color = vec4(0,0,0,1);
	
	if(samples == 1) color = texture(secondPassTex, passTexCoords); 
	else color = vec4(getMultisampleColor(secondPassTex, passTexCoords), 1.0f);
	
	fragmentColor = color;
}

//Contour shading methods

void contoursMain()
{
	fragmentColor = vec4(lineColor, 1.0f);
}

void contoursSolid()
{
	fragmentColor = solidColor;
}

//Cel shading methods

//Antialiasing
float stepmix(float edge0, float edge1, float E, float x)
{
    float T = clamp(0.5 * (x - edge0 + E) / E, 0.0, 1.0);
    return mix(edge0, edge1, T);
}

void celShading()
{
	vec3 N = normalize(EyespaceNormal);
    vec3 L = normalize(LightPosition);
    vec3 Eye = vec3(0, 0, 1);
    vec3 H = normalize(L + Eye);

    float df = max(0.0, dot(N, L));
    float sf = max(0.0, dot(N, H));

    sf = pow(sf, Shininess);

    const float A = 0.1;
    const float B = 0.3;
    const float C = 0.6;
    const float D = 1.0;

    float E = fwidth(df);

    if      (df > A - E && df < A + E) df = stepmix(A, B, E, df);
    else if (df > B - E && df < B + E) df = stepmix(B, C, E, df);
    else if (df > C - E && df < C + E) df = stepmix(C, D, E, df);
    else if (df < A) df = 0.0;
    else if (df < B) df = B;
    else if (df < C) df = C;
    else df = D;

    E = fwidth(sf);

    if (sf > 0.5 - E && sf < 0.5 + E)
    {
        sf = smoothstep(0.5 - E, 0.5 + E, sf);
    }
    else
    {
        sf = step(0.5, sf);
    }

    vec3 color = AmbientMaterial + df * Diffuse + sf * SpecularMaterial;
    fragmentColor = vec4(color, 1.0);
}


void main()
{
if(useSimple)
{
	simpleShading();
}	
if(useHatching)
{
	hatching();
}
if(useGoochFirst)
{
	goochFirstPass();
}
if(useGoochSecond)
{
	goochSecondPass();
}
if(useGoochFinal)
{
	goochFinalPass();
}
if(useContoursMain)
{
	contoursMain();
}
if(useContoursSolid)
{
	contoursSolid();
}
if(useCel)
{
	celShading();
}
}
