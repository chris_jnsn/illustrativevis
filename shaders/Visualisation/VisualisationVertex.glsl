#version 330

//Renderingmethod variable

uniform bool useSimple;
uniform bool useGoochFirst;
uniform bool useGoochSecond;
uniform bool useGoochFinal;
uniform bool useContoursMain;
uniform bool useContoursSolid;
uniform bool useHatching;
uniform bool useCel;
uniform bool useMC;

//General variables

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

uniform mat4 uniformNormal;

mat4 modelViewProjMatrix;

uniform vec4 lightPosition;

//Cel shading variables

uniform vec3 DiffuseMaterial;
out vec3 EyespaceNormal;
out vec3 Diffuse;

//General inputs

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec4 normalAttribute;
layout (location = 2) in vec2 texCoordsAttribute;

//General outputs

out vec3 passPosition;
out vec3 passNormal;

out vec3 passLightPosition;

//Gooch shading variables

out vec2 passTexCoords;

//Contour variables

out VertexData {
	float normalEyeDot;
	vec3 normal;
	vec3 position;
} vertex;



//Simple shading method

void simpleShading()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	//Light
	passLightPosition = (uniformView * lightPosition).xyz;
	
	passNormal = normalize((uniformNormal * normalAttribute).xyz);
	
	passPosition = (uniformView * uniformModel * positionAttribute).xyz;
    gl_Position = modelViewProjMatrix * positionAttribute;

}

//Hatching Method

void hatching()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	//Light
	passLightPosition = (uniformView * lightPosition).xyz;
	
	passNormal = normalize((uniformNormal * normalAttribute).xyz);
	
	passPosition = (uniformView * uniformModel * positionAttribute).xyz;
    gl_Position = modelViewProjMatrix * positionAttribute;

}

//Gooch methods

void goochFirstPass()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	//Light
	passLightPosition = (uniformView * lightPosition).xyz;
	
	passNormal = normalize((uniformNormal * normalAttribute).xyz);
	
	passPosition = (uniformView * uniformModel * positionAttribute).xyz;
    gl_Position = modelViewProjMatrix * positionAttribute;
}

void goochSecondPass()
{
	//Tex Coords
	passTexCoords = texCoordsAttribute;
	
	gl_Position = positionAttribute;
}

void goochFinalPass()
{
	//Tex Coords
	passTexCoords = texCoordsAttribute;
	
	gl_Position = positionAttribute;
}

//Contours Methods

void contoursMain()
{
	vec3 normal = normalize((uniformNormal * normalAttribute).xyz);
	vec3 view = normalize((uniformView * uniformModel * positionAttribute).xyz);
	vertex.normalEyeDot = dot(normal, view);
	
	float test = length(cross(normal, view));
	if(test < 0) vertex.normalEyeDot *= -1.0f;
	
	vertex.normal = normalize(normalAttribute.xyz);	
	vertex.position = positionAttribute.xyz;
	
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
    gl_Position = modelViewProjMatrix * positionAttribute;
}

void contoursSolid()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
    gl_Position = modelViewProjMatrix * positionAttribute;
}

//Cel shading method

void celShading()
{
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	EyespaceNormal = mat3(uniformNormal) * vec3(normalAttribute);
	gl_Position = modelViewProjMatrix * positionAttribute;
	Diffuse = DiffuseMaterial;
}

//Marching cubes gpu method

void mc()
{
	gl_Position = positionAttribute;
}


void main(){
if(useSimple)
{
	simpleShading();
}
if(useHatching)
{
	hatching();
}
if(useGoochFirst)
{
	goochFirstPass();
}
if(useGoochSecond)
{
	goochSecondPass();
}
if(useGoochFinal)
{
	goochFinalPass();
}
if(useContoursMain)
{
	contoursMain();
}
if(useContoursSolid)
{
	contoursSolid();
}
if(useCel)
{
	celShading();
}
if(useMC)
{
	mc();
}
}