#version 420

in vec2 passTexCoords;

out vec4 fragmentColor;

uniform sampler2D secondPassTex;

uniform uint screenWidth;
uniform uint screenHeight;

uniform uint samples;


vec3 getMultisampleColor(sampler2D inputTex, vec2 texCoords){
	float dx = 1.0f / float(screenWidth * samples);
	float dy = 1.0f / float(screenHeight * samples);
		
	vec3 Color = vec3(0,0,0);
		
	for(int y = 0; y < samples; y++){
		for(int x = 0; x < samples; x++){
			Color += texture(inputTex, texCoords.st + vec2(dx * x, dy * y)).rgb;
		}
	}
		
	Color /= (samples * samples);
	return Color;
}

void main(){
	vec4 color = vec4(0,0,0,1);
	
	if(samples == 1) color = texture(secondPassTex, passTexCoords); 
	else color = vec4(getMultisampleColor(secondPassTex, passTexCoords), 1.0f);
	
	fragmentColor = color;
}