#version 420

in vec2 passTexCoords;

out vec4 fragmentColor;

uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D colorTex;

uniform uint screenWidth;
uniform uint screenHeight;

uniform float normalTreshhold;
uniform float lineThickness;

vec4 getDepthValue(int x, int y){
	vec2 texPos = vec2(passTexCoords.x + (x * (lineThickness / float(screenWidth))), passTexCoords.y + (y * (lineThickness / float(screenHeight))));
	
	float depth = texture(depthTex, texPos).r;
	return vec4(vec3(depth), 1);
}

vec4 getLinearizeDepthValue(int x, int y){
	float z = 2.0f * getDepthValue(x,y).r - 1.0f;
	float near = 0.1f;
	float far = 100.0f;
    float c = (2.0 * near * far) / (far + near - z * (far - near));
	
	return vec4(vec3(c), 1);
}

vec4 getNormalValue(int x, int y){
	vec4 color;
	vec2 texPos = vec2(passTexCoords.x + (x * (lineThickness / float(screenWidth))), passTexCoords.y + (y * (lineThickness / float(screenHeight))));
	
	color = texture(normalTex, texPos); 
	
	return color;
}

float getNormalGrayValue(int x, int y){
	vec4 color = getNormalValue(x,y);
	float grayColor = float(color.r) * 0.3f + float(color.g) * 0.6f + float(color.b) * 0.1f;
	return grayColor;
}

vec4 getColorValue(int x, int y){
	vec4 color;
	vec2 texPos = vec2(passTexCoords.x + (x * (1.0f  / float(screenWidth))), passTexCoords.y + (y * (1.0f  / float(screenHeight))));
	
	color = texture(colorTex, texPos); 
	
	return color;
}

vec4 getColorValueMedian(int x, int y){
	vec4 values[9];
	int k = 0;
	
	for (int j = -1; j <= 1; j++) {
		for (int i = -1; i <= 1; i++) {
			values[k] = getColorValue(j,i);
			values[k].w = float(values[k].r) * 0.3f + float(values[k].g) * 0.6f + float(values[k].b) * 0.1f;
			k++;
		}
	}
		
	for (int j = 0; j < 5; j++){
		int min = j;
		for (int l = j + 1; l < 9; l++)
			if (values[l].w < values[min].w) min = l;

		vec4 temp = values[j];
		values[j] = values[min];
		values[min] = temp;
	}
	
	return values[4];
}
		 
vec4 sobelNormals(float treshhold){	
	if(treshhold != 0){
		float gradientX = 0.0f;
		float gradientY = 0.0f;
		
		//Gradient X
		gradientX += getNormalGrayValue(-1, -1);
		gradientX += 2 * getNormalGrayValue(-1, 0);
		gradientX += getNormalGrayValue(-1, 1);
		gradientX -= getNormalGrayValue(1, -1);
		gradientX -= 2 * getNormalGrayValue(1, 0);
		gradientX -= getNormalGrayValue(1, 1);
		
		//Gradient Y
		gradientY += getNormalGrayValue(-1, 1);
		gradientY += 2 * getNormalGrayValue(0, 1);
		gradientY += getNormalGrayValue(1, 1);
		gradientY -= getNormalGrayValue(-1, -1);
		gradientY -= 2 * getNormalGrayValue(0, -1);
		gradientY -= getNormalGrayValue(1, -1);
		
		float mag = sqrt((gradientX * gradientX) + (gradientY * gradientY));
		if(mag >= 1.0f) mag = 1.0f;
		else if(mag < 0.0f) mag = 0.0f;
		
		mag *= treshhold;
		mag = 1.0f - mag;
	
		// if(mag < treshhold) mag = 0.0f;
		// else mag = 1.0f;
	
		return vec4(mag, mag, mag, 1);
	}
	else return vec4(1,1,1,1);
}

vec4 sobelDepth(){	
	float gradientX = 0.0f;
	float gradientY = 0.0f;
		
	//Gradient X
	gradientX += getLinearizeDepthValue(-1, -1).r;
	gradientX += 2 * getLinearizeDepthValue(-1, 0).r;
	gradientX += getLinearizeDepthValue(-1, 1).r;
	gradientX -= getLinearizeDepthValue(1, -1).r;
	gradientX -= 2 * getLinearizeDepthValue(1, 0).r;
	gradientX -= getLinearizeDepthValue(1, 1).r;
		
	//Gradient Y
	gradientY += getLinearizeDepthValue(-1, 1).r;
	gradientY += 2 * getLinearizeDepthValue(0, 1).r;
	gradientY += getLinearizeDepthValue(1, 1).r;
	gradientY -= getLinearizeDepthValue(-1, -1).r;
	gradientY -= 2 * getLinearizeDepthValue(0, -1).r;
	gradientY -= getLinearizeDepthValue(1, -1).r;
		
	float mag = sqrt((gradientX * gradientX) + (gradientY * gradientY));
	if(mag >= 1.0f) mag = 1.0f;
	else if(mag < 0.0f) mag = 0.0f;
		
	mag = 1.0f - mag;
	
	return vec4(mag, mag, mag, 1);
}

vec4 goochShading(){
	vec4 result = getColorValue(0,0) * sobelDepth() * sobelNormals(normalTreshhold);
	return result;
}

void main(){	
	fragmentColor = goochShading();
}