#version 330

in vec3 passPosition;
in vec3 passNormal;

//Light
in vec3 passLightPosition;

layout(location = 0) out vec4 normalColor;
layout(location = 1) out vec4 fragmentColor;

//Light
struct MaterialAttributes
{
	vec3 diffuseColor;
	vec3 specularColor;
	vec3 ambientColor;
	vec3 goochCool;
	vec3 goochWarm;
	float diffuseCoolInfluence;
	float diffuseWarmInfluence;
};

uniform MaterialAttributes matAttr;

void main(){
	normalColor = vec4(passNormal,1);
	
	//compute the light vector as the normalized vector between 
	//the vertex position and the light position:
	vec3 lightVector = normalize(passLightPosition - passPosition);

	//compute the eye vector as the normalized negative vertex 
	//position in camera coordinates:
	vec3 eye = normalize(-passPosition);
    
	//compute the normalized reflection vector using 
	//GLSL's built-in reflect() function:
	vec3 reflection = normalize(reflect(-lightVector, passNormal));

	//comment in to use the phong lighting model:
	float specular = pow(max(dot(reflection, eye), 0), 64);
	float ambient = 0.5;
		
	//comment in to use the phong lighting model:
	vec4 specColor = vec4(specular * matAttr.specularColor, 1);
	vec4 amColor = vec4(ambient  * matAttr.ambientColor, 1);
	
	float blendValue = 0.5f * dot(passNormal, lightVector) + 0.5f;
	vec3 kCoolColor = (matAttr.goochCool + matAttr.diffuseColor * (matAttr.diffuseCoolInfluence)) / (1.0f + matAttr.diffuseCoolInfluence);
	vec3 kWarmColor = (matAttr.goochWarm + matAttr.diffuseColor * (matAttr.diffuseWarmInfluence)) / (1.0f + matAttr.diffuseWarmInfluence);
	
	vec4 goochColor = vec4(mix(kCoolColor, kWarmColor, blendValue), 1);
		
	fragmentColor = goochColor + specColor + amColor;		
}
