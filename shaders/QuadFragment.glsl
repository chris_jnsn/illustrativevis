#version 420

 #extension GL_EXT_gpu_shader4 : enable    //Include support for this extension, which defines usampler2D

in vec2 passTexCoords;
out vec4 fragmentColor;

layout(binding = 0) uniform isampler2D renderTex1;

uniform uint screenWidth;
uniform uint screenHeight;
uniform bool splitScreen;

void main(){
	int data = texture(renderTex1, passTexCoords).a;

	float color = (data + 999.0f) / 3000.0f;
    fragmentColor = vec4(color,color,color,1);
	//fragmentColor = vec4(1,0,0,1);

}
