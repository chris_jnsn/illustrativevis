#version 430
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_image_load_store : enable

layout(R16I,binding=5) 	uniform iimage2D triTableTex;
layout(R16I,binding=6) 	uniform iimage3D dataTex;
layout(R16I,binding=7) 	uniform iimage3D tempTex;

layout(binding=7) buffer vertCount { 
	int VerticeCount [];
};

layout( local_size_x = 1, local_size_y = 1, local_size_z = 1 ) in;

uniform int mode;
uniform int isolevel;

int cellX = 0;
int cellY = 0;
int cellZ = 0;

int cornerValues[8];

ivec3 storePos;

int dataTableValue(int x, int y, int z){
	return imageLoad(dataTex, ivec3(x, y, z)).r;
}

int triTableValue(int x, int y){
	return imageLoad(triTableTex, ivec2(y, x)).r;
}

void initCornerValues(){
	cornerValues[0] = dataTableValue(cellX, cellY, cellZ);
	cornerValues[1] = dataTableValue(cellX+1, cellY, cellZ);
	cornerValues[2] = dataTableValue(cellX+1, cellY+1, cellZ);
	cornerValues[3] = dataTableValue(cellX, cellY+1, cellZ);
	cornerValues[4] = dataTableValue(cellX, cellY, cellZ+1);
	cornerValues[5] = dataTableValue(cellX+1, cellY, cellZ+1);
	cornerValues[6] = dataTableValue(cellX+1, cellY+1, cellZ+1);
	cornerValues[7] = dataTableValue(cellX, cellY+1, cellZ+1);
}

int cellValue(){
	int value = 0;
	
	for(int i = 0; i < 8; i++){
		int div = int(pow(2, i));
		if(cornerValues[i] >= isolevel) value = value | div;
	}
	
	return value;
}

void computeCellValues(){
	cellX = storePos.x;
	cellY = storePos.y;
	cellZ = storePos.z;

	initCornerValues();
	int value = cellValue();
	if(value == 0) return;
	
	int dataValue = 0;
	
	int i = 0;	
	while(true){
		if(triTableValue(value, i) != -1){		
			dataValue += 3;
		}
		else {
			break;
		}
		i += 3;
		if(i > 16) break;
	}
				
	//Image Load and Store
	barrier();
	ivec4 data;
	data.r = dataValue;
	imageStore(tempTex, storePos, data);
}

void countVertices(){
	int count = imageLoad(tempTex, storePos).r;
	
	barrier();
	atomicAdd(VerticeCount[0], count);
}

void main(){
	storePos = ivec3(gl_GlobalInvocationID.xyz);
	
	if(mode == 0) computeCellValues();
	else countVertices();
}