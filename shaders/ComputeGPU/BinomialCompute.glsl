#version 430
#extension GL_ARB_compute_shader : enable
#extension GL_ARB_shader_image_load_store : enable

layout(R16I,binding=6) uniform iimage3D dataTex;
layout(R16I,binding=7) uniform iimage3D tempTex;

uniform int filterMode;

uniform ivec3 imageSize;

uniform int diffValue;

uniform int kernelSize;
uniform int[29] kernel;

ivec3 storePos;

int loopBorder;
int value = 0;

layout( local_size_x = 1, local_size_y = 1, local_size_z = 1 ) in;

//Filter X Direction
void filterDirectionX(){
	for (int j = -loopBorder; j <= loopBorder; j++){
		int tempX = storePos.x;
		if (!(storePos.x + j < 0 || storePos.x + j >= imageSize.x)) tempX = storePos.x + j;
		ivec3 temp = ivec3(tempX, storePos.y, storePos.z);
		ivec4 tempData = imageLoad(dataTex, temp);
		value += kernel[j + loopBorder] * tempData.r;
	}

	ivec4 data = ivec4(0, 0, 0, 0);
	data.r = int(value / diffValue);
				
	//Image Load and Store
	barrier();
	imageStore(tempTex, storePos, data);
}

//Filter Y Direction
void filterDirectionY(){
	for (int j = -loopBorder; j <= loopBorder; j++){
		int tempY = storePos.y;
		if (!(storePos.y + j < 0 || storePos.y + j >= imageSize.y)) tempY = storePos.y + j;
		ivec3 temp = ivec3(storePos.x, tempY, storePos.z);
		ivec4 tempData = imageLoad(tempTex, temp);
		value += kernel[j + loopBorder] * tempData.r;
	}

	ivec4 data = ivec4(0, 0, 0, 0);
	data.r = int(value / diffValue);
				
	//Image Load and Store
	barrier();
	imageStore(dataTex, storePos, data);
}

//Filter Z Direction
void filterDirectionZ(){
	for (int j = -loopBorder; j <= loopBorder; j++){
		int tempZ = storePos.z;
		if (!(storePos.z + j < 0 || storePos.z + j >= imageSize.z)) tempZ = storePos.z + j;
		ivec3 temp = ivec3(storePos.x, storePos.y, tempZ);
		ivec4 tempData = imageLoad(dataTex, temp);
		value += kernel[j + loopBorder] * tempData.r;
	}

	ivec4 data = ivec4(0, 0, 0, 0);
	data.r = int(value / diffValue);
			
	//Image Load and Store
	barrier();
	imageStore(tempTex, storePos, data);
}

//Transfer Data
void transferTempTexToDataTex(){
	//Image Load and Store
	barrier();
	ivec4 data = imageLoad(tempTex, storePos);
	imageStore(dataTex, storePos, data);
}

void main(){
	storePos = ivec3(gl_GlobalInvocationID.xyz);
	loopBorder = (kernelSize - 1) / 2;

	if(filterMode == 0)filterDirectionX();
	else if(filterMode == 1) filterDirectionY();
	else if(filterMode == 2) filterDirectionZ();
	else transferTempTexToDataTex();
}