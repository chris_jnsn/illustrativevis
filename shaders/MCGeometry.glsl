#version 420
#extension GL_EXT_gpu_shader4 : enable

layout (points) in; 
layout (triangle_strip, max_vertices = 16) out;

out vec3 passNormal;
out vec3 passPosition;

layout(binding = 0) uniform isampler3D dataTableTex;
layout(binding = 1) uniform isampler2D triTableTex;

uniform ivec3 gridSize;
uniform int isolevel;

vec4 corner[8];
vec4 edgePositions[12];
int cornerValues[8];

int cellX, cellY, cellZ;

int dataTableValue(int x, int y, int z){
	return texelFetch3D(dataTableTex, ivec3(x, y, z), 0).a;
}

int triTableValue(int x, int y){
	return texelFetch2D(triTableTex, ivec2(y, x), 0).a;
}

vec3 vertexInterp(vec3 v0, vec3 v1, int v0Val, int v1Val){
	return mix(v0, v1, (isolevel - v0Val) / float(v1Val - v0Val));
}

void initCornerValues(){
	cornerValues[0] = dataTableValue(cellX, cellY, cellZ);
	cornerValues[1] = dataTableValue(cellX+1, cellY, cellZ);
	cornerValues[2] = dataTableValue(cellX+1, cellY+1, cellZ);
	cornerValues[3] = dataTableValue(cellX, cellY+1, cellZ);
	cornerValues[4] = dataTableValue(cellX, cellY, cellZ+1);
	cornerValues[5] = dataTableValue(cellX+1, cellY, cellZ+1);
	cornerValues[6] = dataTableValue(cellX+1, cellY+1, cellZ+1);
	cornerValues[7] = dataTableValue(cellX, cellY+1, cellZ+1);
}

int cellValue(){
	int value = 0;
	
	for(int i = 0; i < 8; i++){
		int div = int(pow(2, i));
		if(cornerValues[i] >= isolevel) value = value | div;
	}
	
	return value;
}

float dataTableValueInterpolated(float x, float y, float z){
    float tx = x - int(x);
    float ty = y - int(y);
    float tz = z - int(z);

    int corner1 = dataTableValue(int(x), int(y), int(z));
    int corner2 = dataTableValue(int(x+1), int(y), int(z));

    int corner3 = dataTableValue(int(x), int(y+1), int(z));
    int corner4 = dataTableValue(int(x+1),int(y+1), int(z));

    int corner5 = dataTableValue(int(x), int(y), int(z+1));
    int corner6 = dataTableValue(int(x+1), int(y), int(z+1));

    int corner7 = dataTableValue(int(x), int(y+1), int(z+1));
    int corner8 = dataTableValue(int(x+1), int(y+1), int(z+1));

    float interEdge1 = (1.0f - tx) * corner1 + tx * corner2;
    float interEdge2 = (1.0f - tx) * corner3 + tx * corner4;
    float interEdge3 = (1.0f - tx) * corner5 + tx * corner6;
    float interEdge4 = (1.0f - tx) * corner7 + tx * corner8;

    float interEdge5 = (1.0f - ty) * interEdge1 + ty * interEdge2;
    float interEdge6 = (1.0f - ty) * interEdge3 + ty * interEdge4;

    float value = (1.0f - tz) * interEdge5 + tz * interEdge6;

    return value;
}

vec3 gradient(vec3 pos){
	vec3 position;
	position.x = ((pos.x + 1.0f) * (gridSize.x / 2.0f));
	position.y = ((pos.y + 1.0f) * (gridSize.y / 2.0f));
	position.z = ((1.0f - pos.z) * (gridSize.z / 2.0f));
	
	float value = 1.0f;
	
    vec3 grad;

    if(position.x-1 >= 0 && position.x+1 < gridSize.x && position.y-1 >= 0
            && position.y+1 < gridSize.y && position.z-1 >= 0 && position.z+1 < gridSize.z){
        grad = vec3(
                dataTableValueInterpolated((position.x - value), position.y, position.z) - dataTableValueInterpolated((position.x + value), position.y, position.z),
                dataTableValueInterpolated(position.x, (position.y - value), position.z) - dataTableValueInterpolated(position.x, (position.y + value), position.z),
                dataTableValueInterpolated(position.x, position.y, (position.z + value)) - dataTableValueInterpolated(position.x, position.y, (position.z - value)));
    } else {
        grad = vec3(0,0,0);
    }
	
	return grad;
}

void passVertex(int value, int i){
	vec4 pos = edgePositions[triTableValue(value, i)];
	gl_Position = pos;
	passPosition = pos.xyz;	

	vec3 grad = gradient(pos.xyz);
	vec3 normal = normalize(grad);
	passNormal = normal;
	
	EmitVertex();
}

void drawVertices(){
	initCornerValues();
	int value = cellValue();
	if(value == 0) return;

	float x0 = -1.0f + ((2.0f / gridSize.x) * cellX);
	float x1 = -1.0f + ((2.0f / gridSize.x) * (cellX + 1.0f));
	
	float y0 = -1.0f + ((2.0f / gridSize.y) * cellY);
	float y1 = -1.0f + ((2.0f / gridSize.y) * (cellY + 1.0f));
	
	float z0 = 1.0f - ((2.0f / gridSize.z) * cellZ);
	float z1 = 1.0f - ((2.0f / gridSize.z) * (cellZ + 1.0f));
	
	corner[0] = vec4(x0, y0, z0, 1.0f);
	corner[1] = vec4(x1, y0, z0, 1.0f);
	corner[2] = vec4(x1, y1, z0, 1.0f);
	corner[3] = vec4(x0, y1, z0, 1.0f);
	
	corner[4] = vec4(x0, y0, z1, 1.0f);
	corner[5] = vec4(x1, y0, z1, 1.0f);
	corner[6] = vec4(x1, y1, z1, 1.0f);
	corner[7] = vec4(x0, y1, z1, 1.0f);	
	
	edgePositions[0] = vec4(vertexInterp(corner[0].xyz, corner[1].xyz, cornerValues[0], cornerValues[1]),1);
	edgePositions[1] = vec4(vertexInterp(corner[1].xyz, corner[2].xyz, cornerValues[1], cornerValues[2]),1);
	edgePositions[2] = vec4(vertexInterp(corner[2].xyz, corner[3].xyz, cornerValues[2], cornerValues[3]),1);
	edgePositions[3] = vec4(vertexInterp(corner[3].xyz, corner[0].xyz, cornerValues[3], cornerValues[0]),1);
	
	edgePositions[4] = vec4(vertexInterp(corner[4].xyz, corner[5].xyz, cornerValues[4], cornerValues[5]),1);
	edgePositions[5] = vec4(vertexInterp(corner[5].xyz, corner[6].xyz, cornerValues[5], cornerValues[6]),1);
	edgePositions[6] = vec4(vertexInterp(corner[6].xyz, corner[7].xyz, cornerValues[6], cornerValues[7]),1);
	edgePositions[7] = vec4(vertexInterp(corner[7].xyz, corner[4].xyz, cornerValues[7], cornerValues[4]),1);
	
	edgePositions[8] = vec4(vertexInterp(corner[0].xyz, corner[4].xyz, cornerValues[0], cornerValues[4]),1);
	edgePositions[9] = vec4(vertexInterp(corner[1].xyz, corner[5].xyz, cornerValues[1], cornerValues[5]),1);
	edgePositions[10] = vec4(vertexInterp(corner[2].xyz, corner[6].xyz, cornerValues[2], cornerValues[6]),1);
	edgePositions[11] = vec4(vertexInterp(corner[3].xyz, corner[7].xyz, cornerValues[3], cornerValues[7]),1);
	
	int i = 0;	
	while(true){
		if(triTableValue(value, i) != -1){		
			//Vertex 1
			passVertex(value, i);

			//Vertex 2
			passVertex(value, i+1);

			//Vertex 3
			passVertex(value, i+2);
		
			EndPrimitive();	
		}
		else {
			break;
		}
		i += 3;
		if(i > 16) break;
	}
}

void main(){
	cellX = int((gl_in[0].gl_Position.x + 1.0f) * (gridSize.x / 2.0f));
	cellY = int((gl_in[0].gl_Position.y + 1.0f) * (gridSize.y / 2.0f));
	cellZ = int((1.0f - gl_in[0].gl_Position.z) * (gridSize.z / 2.0f));
	
	drawVertices();
}