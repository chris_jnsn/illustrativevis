#version 330

in vec3 passPosition;
in vec3 passNormal;

uniform int uniformMode;

//Light
in vec3 passLightPosition;

out vec4 fragmentColor;

//Light
vec3 lightDiffuse = vec3(1,1,1);
vec3 lightSpecular = vec3(1,1,1);
vec3 lightAmbient = vec3(0.5f, 0.5f, 0.5f);

void main(){
	if(uniformMode == 0) fragmentColor = vec4(passPosition,1);
	else if(uniformMode == 1) fragmentColor = vec4(passNormal,1);
	else {
		//compute the light vector as the normalized vector between 
		//the vertex position and the light position:
		vec3 lightVector = normalize(passLightPosition - passPosition);

		//compute the eye vector as the normalized negative vertex 
		//position in camera coordinates:
		vec3 eye = normalize(-passPosition);
    
		//compute the normalized reflection vector using 
		//GLSL's built-in reflect() function:
		vec3 reflection = normalize(reflect(-lightVector, passNormal));

		//comment in to use the phong lighting model:
		float diffuse = max(dot(passNormal, lightVector), 0);
		float specular = pow(max(dot(reflection, eye), 0), 250);
		float ambient = 0.5;
		
		//comment in to use the phong lighting model:
		vec4 diffuseColor = vec4(diffuse * lightDiffuse, 1);
		vec4 specularColor = vec4(specular * lightSpecular, 1);
		vec4 ambientColor = vec4(ambient  * lightAmbient, 1);
		
		fragmentColor = diffuseColor + specularColor + ambientColor;		
	}
}
