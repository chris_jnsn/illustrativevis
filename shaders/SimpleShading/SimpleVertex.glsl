#version 330

layout (location = 0) in vec4 positionAttribute;
layout (location = 1) in vec4 normalAttribute;

out vec3 passPosition;
out vec3 passNormal;

//Light
out vec3 passLightPosition;

uniform mat4 uniformModel;
uniform mat4 uniformView;
uniform mat4 uniformProjection;

uniform mat4 uniformNormal;

mat4 modelViewProjMatrix;

//Light
uniform vec4 lightPosition;

void main(){
	modelViewProjMatrix = uniformProjection * uniformView * uniformModel;
	
	//Light
	passLightPosition = (uniformView * lightPosition).xyz;
	
	passNormal = normalize((uniformNormal * normalAttribute).xyz);
	
	passPosition = (uniformView * uniformModel * positionAttribute).xyz;
    gl_Position = modelViewProjMatrix * positionAttribute;
}