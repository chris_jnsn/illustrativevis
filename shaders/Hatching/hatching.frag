#version 330

in vec3 passPosition;
in vec3 passNormal;

//hatching
uniform mat2 thetaRotation;
uniform int patternSize;
uniform float minIntensity;
uniform vec4 fillColor;
uniform vec4 hatchingColor;

//Light
in vec3 passLightPosition;

out vec4 fragmentColor;

vec4 hatching()
{
	//implemented following paper 'Illustrative Display of Hidden Iso-Surface Structures'
	//http://www.gris.uni-tuebingen.de/people/staff/fischer/janfischer.com/publications/Fischer05_IllusTrans.pdf

	//calculate intensity I_p of current fragment
	vec3 lightVector = normalize(passLightPosition - passPosition);
	float I_p = max(dot(passNormal,lightVector),0);

	//rotate fragment coords around user defined angle theta
	vec2 thetaFragCoords = thetaRotation * gl_FragCoord.xy;

	//calculate dither coords from fragment coords
	vec2 ditherCoords;
	ditherCoords.s = mod(thetaFragCoords.x,patternSize) / patternSize;
	ditherCoords.t = mod(thetaFragCoords.y,patternSize) / patternSize;

	//calculate halftoning threshold tau
	float thresholdTau;

	if(ditherCoords.s < minIntensity)
	{
		thresholdTau = minIntensity * ditherCoords.t;
	}
	else
	{
		thresholdTau = (1 - minIntensity) * ditherCoords.s + minIntensity;
	}

	//calculate intensity I_hatching for current fragment
	if(I_p < thresholdTau)
	{
		return hatchingColor;
	}

	else
	{
		return fillColor;
	}


}

void main()
{
		fragmentColor = hatching();
}
