DATA LOADING:

Put the dataset folders directly into the DICOM folder

Open up the dicomPresets.txt and add one entry for each dataset, that should be loadable.
Presets have to be defined in the following way:

DICOM(Name,Path,Slice Count)

You can load DICOM Datasets from: http://www.osirix-viewer.com/datasets/

Example Dataset:

DICOM(Phenix;Phenix/COU IV/;361) (DL: http://www.osirix-viewer.com/datasets/DATA/PHENIX.zip)
