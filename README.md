**Illustrative Visualization Assignment MedCV**

- Supported systems are Windows (MinGW und MSVC) und Linux
- Internet connection has to be available during the whole build process, since some dependency packages are downloaded (~300 MB)
- be sure to have git installed on your system for the build process, as its needed to download and build the dependencies



**Build on Windows**

- Create project files with Cmake and build inside your respective IDE


**Build on Linux**

- Install some standard opengl packages:

  > $ sudo apt-get install xorg-dev

  > $ sudo apt-get install libglu1-mesa-dev

- Create project files with Cmake and build inside your respective IDE
